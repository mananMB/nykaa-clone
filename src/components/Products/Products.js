import React from "react";
import "./Products.css";
import { Link } from "react-router-dom";

function Products(props) {
  const products = () => {
    if (props.sortOrder === "Price: High to Low") {
      return props.products.sort((productA, productB) => {
        return productB.price - productA.price;
      });
    }
    if (props.sortOrder === "Price: Low to High") {
      return props.products.sort((productA, productB) => {
        return productA.price - productB.price;
      });
    }
    if (props.sortOrder === "Name") {
      return props.products.sort((productA, productB) => {
        return ("" + productA.title).localeCompare(productB.title);
      });
    }
    if (props.sortOrder === "Discount") {
      return props.products.sort((productA, productB) => {
        return productB.discount - productA.discount;
      });
    }
    if (props.sortOrder === "Customer Top Rated") {
      return props.products.sort((productA, productB) => {
        return productB.rating.rate - productA.rating.rate;
      });
    }
    return props.products;
  };
  return (
    <div
      className={
        "d-flex flex-wrap gap-3 justify-content-center align-items-center"
      }
      style={{ width: props.width || "100%" }}
    >
      {products().length === 0 && <h2> No Products found</h2>}
      {products().map((product) => {
        return (
          <div
            key={product.id}
            className="card product-card col-12"
            style={{ width: "18rem", height: "35rem" }}
          >
            <div
              className={"d-flex"}
              style={{
                flexWrap: "nowrap",
                paddingLeft: "0.8rem",
                paddingTop: "0.6rem",
                fontSize: "0.8rem",
                fontWeight: 400,
              }}
            >
              {product.featured && (
                <div
                  style={{
                    color: "red",
                    width: "max-content",
                    paddingRight: "1rem",
                  }}
                >
                  FEATURED
                </div>
              )}
              {product.bestseller && (
                <div style={{ color: "green", width: "max-content" }}>
                  BESTSELLER
                </div>
              )}
              {!(product.featured && product.bestseller) && <div>&nbsp;</div>}
            </div>
            <img src={product.image} className="card-img-top" alt="..." />
            <div className="card-body" style={{ textAlign: "center" }}>
              <Link
                to={"/product/" + product.id}
                style={{ textDecoration: "none" }}
              >
                <p
                  className="card-title"
                  style={{ fontWeight: "500", textTransform: "capitalize" }}
                >
                  {product.title}
                </p>
              </Link>
              <p
                className="text-muted"
                style={{ fontWeight: "500", textTransform: "capitalize" }}
              >
                {product.brand}
              </p>
              <div className="card-text d-flex flex-column gap-2 pt-2">
                <span>
                  <s className={"text-muted"}>
                    MRP: ₹{product.mrp.toLocaleString("en-IN")}
                  </s>{" "}
                  ₹{(product.price * 1).toLocaleString("en-IN")} |{" "}
                  <span style={{ color: "green" }}>
                    {product.discount}% off
                  </span>
                </span>
                <div style={{ color: "var(--nykaa-pink)" }}>
                  {product.freeGift ? "Enjoy Free Gift" : <div>&nbsp;</div>}
                </div>
                <div className="rating">
                  {product.rating.rate}
                  {`(${product.rating.count}+)`}
                </div>
              </div>
            </div>
            <div className={"d-flex w-100 h-25 product-card-button-container"}>
              <div
                className={
                  "col-3 d-flex align-items-center justify-content-center fs-2"
                }
              >
                <i className="fa-regular fa-heart"></i>
              </div>
              <button
                className={
                  "btn btn-primary col-9 d-flex align-items-center justify-content-center"
                }
                style={{
                  borderRadius: "unset",
                  borderBottomRightRadius: "0.3rem",
                }}
                onClick={() => {
                  props.updateCart(product);
                }}
              >
                Add to Bag
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default Products;
