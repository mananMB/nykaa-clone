import React from "react";
import products from "../../product-data";

class Product extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    const product = products.find((product) => {
      return product.id === this.props.id;
    });

    const isProductInCart = () => {
      return this.props.bag.hasOwnProperty(this.props.id);
    };

    return (
      <div
        className={"d-flex flex-wrap align-items-center justify-content-center"}
      >
        <div
          className="card"
          style={{ width: "90%", height: "34rem", marginTop: "4rem" }}
        >
          <div className="card-body  d-flex">
            {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
            <img
              src={product.image}
              className="card-img-top"
              alt="product-image"
              style={{
                width: "auto",
                height: "32rem",
                borderRadius: 0,
                borderRight: "1px solid silver",
                paddingRight: "1rem",
              }}
            />
            <div className={"ps-4 py-2"}>
              <h4
                className="card-title"
                style={{ textTransform: "capitalize" }}
              >
                {product.title}
              </h4>
              <p
                className={"text-muted"}
                style={{ textTransform: "capitalize" }}
              >
                {product.brand}
              </p>
              <p className="card-text rating fs-6">
                {product.rating.rate}
                {`(${product.rating.count}+)`}
              </p>
              <span className={"fs-5"}>
                <s className={"text-muted"}>
                  MRP: ₹{product.mrp.toLocaleString("en-IN")}
                </s>{" "}
                ₹{Number(product.price).toLocaleString("en-IN")} |{" "}
                <span style={{ color: "green" }}>{product.discount}% off</span>
              </span>
              <p className={"text-muted"}>inclusive of all taxes</p>
              <div className={"description"}>{product.description}</div>
              {isProductInCart() ? (
                <div
                  className="btn-group mt-5"
                  role="group"
                  aria-label="Basic mixed styles"
                >
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => {
                      if (this.props.bag[this.props.id].count > 1) {
                        this.props.updateCart(
                          this.props.bag[this.props.id],
                          "removeOne"
                        );
                      } else {
                        this.props.updateCart(this.props.id, "remove");
                      }
                    }}
                  >
                    -
                  </button>
                  <div
                    className={
                      "d-flex align-items-center justify-content-center px-3"
                    }
                  >
                    {this.props.bag[this.props.id].count}
                  </div>
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => {
                      this.props.updateCart(this.props.bag[this.props.id]);
                    }}
                  >
                    +
                  </button>
                </div>
              ) : (
                <button
                  className={"btn btn-primary mt-5"}
                  style={{
                    borderRadius: 0,
                    width: "14rem",
                    height: "3rem",
                  }}
                  onClick={() => {
                    this.props.updateCart(product);
                  }}
                >
                  Add to Bag
                </button>
              )}
            </div>
          </div>
        </div>
        <img
          className={"w-75 pb-3 py-4"}
          src="https://images-static.nykaa.com/uploads/66350e64-13d1-4347-8842-fc3638b99b38.jpg?tr=w-2400,cm-pad_resize"
          alt={"product-banner"}
        />
      </div>
    );
  }
}

export default Product;
