import React from "react";
import { Link } from "react-router-dom";

function CheckoutHeader(props) {
  return (
    <div
      className={"d-flex align-items-center justify-content-start px-5"}
      style={{
        height: "4rem",
        background: "white",
        borderBottom: "1px solid silver",
      }}
    >
      <Link to={"/"}>
        <img
          src={
            "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/nykaa-logo.svg"
          }
        />
      </Link>
      <div
        className={"d-flex gap-4 justify-content-center align-items-center"}
        style={{
          fontSize: "16px",
          position: "absolute",
          top: "-0.012%",
          left: "50%",
          transform: "translate(-50%, 10%)",
        }}
      >
        <div className={"d-flex gap-4"}>
          <img src={props.logo1} />
          <p style={{ marginTop: "13px" }}>Sign&nbsp;Up</p>
        </div>
        <hr style={{ width: "50px" }} />
        <div className={"d-flex gap-4"}>
          <img src={props.logo2} />
          <p style={{ marginTop: "13px" }}>Address</p>
        </div>
        <hr style={{ width: "50px" }} />
        <div className={"d-flex gap-4"}>
          <img src={props.logo3} />
          <p style={{ marginTop: "13px" }}>Payment</p>
        </div>
        <hr />
      </div>
    </div>
  );
}

export default CheckoutHeader;
