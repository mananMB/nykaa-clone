import React from "react";
import { Link } from "react-router-dom";

function OrderCreatedPage(props) {
  return (
    <div
      style={{ minHeight: "67.6vh" }}
      className={
        "d-flex flex-column align-items-center justify-content-center gap-3"
      }
    >
      <div style={{ width: "5rem", height: "5rem" }}>
        <img
          src={
            "https://upload.wikimedia.org/wikipedia/commons/3/3b/Eo_circle_green_checkmark.svg"
          }
        />
      </div>
      <h1>Order Created</h1>
      <p>Order details will be sent to your registered mobile number</p>
      <Link to={"/"}>
        <button className={"btn btn-primary"}>Go to Homepage</button>
      </Link>
    </div>
  );
}

export default OrderCreatedPage;
