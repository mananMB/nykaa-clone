import React from "react";
import BagAccordion from "./BagAccordion";
import validator from "validator/es";
import { Link } from "react-router-dom";

class AddressBody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pincode: "",
      addressNumber: "",
      address: "",
      name: "",
      phone: "",
      email: "",
    };
  }
  validatePincode = () => {
    return (
      validator.isNumeric(this.state.pincode) &&
      validator.isLength(this.state.pincode, { min: 6, max: 6 })
    );
  };

  validateAddressNumber = () => {
    return !validator.isEmpty(this.state.addressNumber);
  };

  validateAddress = () => {
    return validator.isLength(this.state.address, { min: 5 });
  };

  validateName = () => {
    return (
      validator.isLength(this.state.name, { min: 5 }) &&
      validator.isAlpha(this.state.name)
    );
  };

  validatePhoneNumber = () => {
    return (
      validator.isNumeric(this.state.phone) &&
      validator.isLength(this.state.phone, { min: 10, max: 10 })
    );
  };

  validateEmail = () => {
    return validator.isEmail(this.state.email);
  };

  hashCode = (str) => {
    let hash = 0;
    for (let i = 0, len = str.length; i < len; i++) {
      let chr = str.charCodeAt(i);
      hash = (hash << 5) - hash + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  };

  allFieldsValid = () => {
    return (
      this.validatePincode() &&
      this.validateEmail() &&
      this.validatePhoneNumber() &&
      this.validateName() &&
      this.validateAddress() &&
      this.validateAddressNumber()
    );
  };

  render() {
    return (
      <div className={"address-body h-100"} style={{ padding: "5rem 10%" }}>
        <div>
          <h2 className={"fw-bolder"}>Choose Address</h2>
          <p>
            Detailed address will help our delivery partner reach your doorstep
            quickly
          </p>
        </div>
        <div className={"d-flex justify-content-between"}>
          <div className={"d-flex gap-3 flex-wrap"}>
            <button
              className="card d-flex flex-column fw-bold align-items-center justify-content-center gap-3 fs-5 address-placeholder-card"
              data-bs-toggle="offcanvas"
              data-bs-target="#addressCanvas"
              style={{
                width: "24rem",
                height: "10rem",
                borderStyle: "dashed",
                color: "var(--nykaa-pink)",
              }}
            >
              <div
                className={"d-flex"}
                style={{ textAlign: "center", color: "var(--nykaa-pink)" }}
              >
                <i className="fa-solid fa-plus"></i>
              </div>
              <div
                className={"d-flex"}
                style={{ textAlign: "center", color: "var(--nykaa-pink)" }}
              >
                Add New Address
              </div>
            </button>
            {this.props.address ? (
              this.props.address.map((address) => {
                return (
                  <div
                    className="card d-flex flex-column align-items-start justify-content-center"
                    style={{
                      width: "24rem",
                      height: "10rem",
                      padding: "2rem",
                    }}
                  >
                    <div>{address.name}</div>
                    <div className={"text-muted"}>
                      {address.addressNumber} {address.address}-
                      {address.pincode}
                    </div>
                    <div className={"text-muted"}>{address.phone}</div>
                    <Link to={"/payment"} params={{ addressId: address.id }}>
                      <div
                        className={"btn btn-primary mt-2"}
                        style={{ fontSize: "0.8rem" }}
                        onClick={() => {
                          this.props.updateDeliveryAddress(address);
                        }}
                      >
                        Deliver here <i className="fa-solid fa-arrow-right"></i>
                      </div>
                    </Link>
                  </div>
                );
              })
            ) : (
              <div></div>
            )}
          </div>

          <BagAccordion
            key={"AccordianStep2"}
            updateCart={this.props.updateCart}
            bag={this.props.bag}
          />
        </div>
        <div
          className="offcanvas offcanvas-end"
          tabIndex="-1"
          id="addressCanvas"
          aria-labelledby="addressCanvasLabel"
        >
          <div className="offcanvas-header">
            <div className={"d-flex"}>
              <button
                type="button"
                className="btn-close mt-0 me-2"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
              ></button>
              <h5 className="offcanvas-title" id="addressCanvasLabel">
                New Address
              </h5>
            </div>
          </div>
          <div className="offcanvas-body">
            <div className={"fw-bold fs-4 pb-2"}>Address</div>
            <form>
              <div className="mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="address-pincode"
                  placeholder={"Pincode"}
                  onChange={(event) => {
                    this.setState({
                      pincode: event.target.value,
                    });
                  }}
                />
                {!this.validatePincode() && (
                  <div
                    id="pincodeHelp"
                    className="form-text"
                    style={{ color: "red" }}
                  >
                    Please enter a valid pincode.
                  </div>
                )}
              </div>
              <div className="mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="address-number"
                  placeholder={"House/Flat/Office No."}
                  onChange={(event) => {
                    this.setState({
                      addressNumber: event.target.value,
                    });
                  }}
                />
                {!this.validateAddressNumber() && (
                  <div
                    id="addressNumberHelp"
                    className="form-text"
                    style={{ color: "red" }}
                  >
                    This field is required.
                  </div>
                )}
              </div>
              <div className="mb-3">
                <textarea
                  className="form-control"
                  id="address-area"
                  placeholder={"Road Name/Area/Colony"}
                  onChange={(event) => {
                    this.setState({
                      address: event.target.value,
                    });
                  }}
                />
                {!this.validateAddress() && (
                  <div
                    id="addressHelp"
                    className="form-text"
                    style={{ color: "red" }}
                  >
                    Area should have at least 5 characters
                  </div>
                )}
              </div>
              <div className={"fw-bold fs-4 pt-4"}>Contact</div>
              <p style={{ fontSize: "0.8rem" }}>
                Information provided here will be used to contact you for
                delivery updates
              </p>
              <div className="mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  placeholder={"Name"}
                  onChange={(event) => {
                    this.setState({
                      name: event.target.value,
                    });
                  }}
                />
                {!this.validateName() && (
                  <div
                    id="nameHelp"
                    className="form-text"
                    style={{ color: "red" }}
                  >
                    Please enter a valid name.
                  </div>
                )}
              </div>
              <div className="mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="phone-number"
                  placeholder={"Phone"}
                  onChange={(event) => {
                    this.setState({
                      phone: event.target.value,
                    });
                  }}
                />
                {!this.validatePhoneNumber() && (
                  <div
                    id="phoneNumberHelp"
                    className="form-text"
                    style={{ color: "red" }}
                  >
                    Please enter a valid phone number.
                  </div>
                )}
              </div>
              <div className="mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  placeholder={"Email ID (optional)"}
                  onChange={(event) => {
                    this.setState({
                      email: event.target.value,
                    });
                  }}
                />
                {!this.validateEmail() && (
                  <div
                    id="emailHelp"
                    className="form-text"
                    style={{ color: "red" }}
                  >
                    Please enter a valid email ID.
                  </div>
                )}
              </div>
              {this.allFieldsValid() ? (
                <button
                  type="submit"
                  className="btn btn-primary"
                  data-bs-toggle="offcanvas"
                  data-bs-target="#addressCanvas"
                  onClick={(event) => {
                    event.preventDefault();
                    this.props.updateAddress({
                      id: this.hashCode(
                        this.state.name +
                          this.state.email +
                          this.state.phone +
                          this.state.pincode +
                          this.state.address +
                          this.state.addressNumber
                      ),
                      name: this.state.name,
                      email: this.state.email,
                      phone: this.state.phone,
                      pincode: this.state.pincode,
                      address: this.state.address,
                      addressNumber: this.state.addressNumber,
                    });
                  }}
                >
                  Save
                </button>
              ) : (
                <button disabled className="btn btn-primary">
                  Save
                </button>
              )}
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AddressBody;
