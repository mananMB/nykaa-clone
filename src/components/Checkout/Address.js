import React from "react";
import CheckoutHeader from "./CheckoutHeader";
import AddressBody from "./AddressBody";

function Address(props) {
  return (
    <div
      className={"min-vw-100 min-vh-100"}
      style={{ top: 0, bottom: 0, position: "fixed", background: "white" }}
    >
      <CheckoutHeader
        logo1={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/tick-icon.svg"
        }
        logo2={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/edit-icon.svg"
        }
        logo3={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/step-3-icon.svg"
        }
      />
      <AddressBody address={props.address} updateCart={props.updateCart} bag={props.bag} updateAddress={props.updateAddress} updateDeliveryAddress={props.updateDeliveryAddress}/>
    </div>
  );
}

export default Address;
