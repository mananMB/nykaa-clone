import React from "react";
import CheckoutHeader from "./CheckoutHeader";
import AddressBody from "./AddressBody";
import PaymentBody from "./PaymentBody";

function Payment(props) {
  console.log(props.match);
  return (
    <div
      className={"min-vw-100 min-vh-100"}
      style={{ top: 0, bottom: 0, position: "fixed", background: "white" }}
    >
      <CheckoutHeader
        logo1={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/tick-icon.svg"
        }
        logo2={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/tick-icon.svg"
        }
        logo3={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/edit-icon.svg"
        }
      />
      <PaymentBody
        deliveryAddress={props.deliveryAddress}
        address={props.address}
        updateCart={props.updateCart}
        bag={props.bag}
        clearCart={props.clearCart}
        updateAddress={props.updateAddress}
      />
    </div>
  );
}

export default Payment;
