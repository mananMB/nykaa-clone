import React from "react";
import CheckoutHeader from "./CheckoutHeader";
import CheckoutBody from "./CheckoutBody";
import "./Checkout.css";

function Checkout(props) {
  return (
    <div
      className={"min-vw-100 min-vh-100"}
      style={{ top: 0, bottom: 0, position: "fixed", background: "white" }}
    >
      <CheckoutHeader
        logo1={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/edit-icon.svg"
        }
        logo2={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/step-2-icon.svg"
        }
        logo3={
          "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/step-3-icon.svg"
        }
      />
      <CheckoutBody updateCart={props.updateCart} bag={props.bag} />
    </div>
  );
}

export default Checkout;
