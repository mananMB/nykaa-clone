import React, { Component } from "react";
import BagAccordion from "./BagAccordion";
import cardValidator from "card-validator";
import { Link } from "react-router-dom";

const titleCase = (str) => {
  str = str
    .toLowerCase()
    .split(" ")
    .map(function (word) {
      return word.replace(word[0], word[0].toUpperCase());
    });
  return str.join(" ");
};

class PaymentBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardNumber: "",
      cardExpiry: "",
      cardCvv: null,
    };
  }
  validateCard = () => {
    return cardValidator.number(this.state.cardNumber);
  };

  validateExpiry = () => {
    return (
      cardValidator.expirationMonth(this.state.cardExpiry.split("/")[0])
        .isValid &&
      cardValidator.expirationYear(this.state.cardExpiry.split("/")[1]).isValid
    );
  };

  validateCode = () => {
    const codeLength =
      (this.validateCard().card && this.validateCard().card.code.size) || 3;
    return cardValidator.cvv(this.state.cardCvv, codeLength);
  };

  allFieldsValid = () => {
    return (
      this.validateCard().isValid &&
      this.validateExpiry() &&
      this.validateCode().isValid
    );
  };

  render() {
    return (
      <div className={"payment-body h-100"} style={{ padding: "5rem 10%" }}>
        <div>
          <h2 className={"fw-bolder"}>Payment</h2>
          <p>Enter Card Details</p>
        </div>
        <div className={"d-flex justify-content-between"}>
          <div className={"card"} style={{ width: "40rem", height: "20rem" }}>
            <div className="card-body">
              <h5 className="card-title mb-0">Credit/Debit Card</h5>
              <div className="card-text">
                We accept all types of card payments
              </div>
              <form>
                <div className="mb-3 col-12">
                  <input
                    type="text"
                    placeholder={"Card Number"}
                    className="form-control col-12"
                    id="card-number"
                    aria-describedby="cardNumberHelp"
                    onChange={(event) => {
                      this.setState({
                        cardNumber: event.target.value,
                      });
                    }}
                  />
                  {this.validateCard().isPotentiallyValid &&
                    this.validateCard().card && (
                      <div id="cardNumberHelp" className="form-text">
                        {titleCase(this.validateCard().card.type)}
                      </div>
                    )}
                  {!this.validateCard().isPotentiallyValid && (
                    <div id="cardNumberHelp" className="form-text">
                      Please enter a valid card number
                    </div>
                  )}
                </div>
                <div className={"d-flex gap-4"}>
                  <div className="mb-3 col-3">
                    <input
                      type="text"
                      placeholder={"Card Expiry"}
                      className="form-control"
                      id="cardExpiry"
                      onChange={(event) => {
                        this.setState({
                          cardExpiry: event.target.value,
                        });
                      }}
                    />
                    {!this.validateExpiry().isPotentiallyValid && (
                      <div id="cardExpiryHelp" className="form-text">
                        Please enter a valid expiry date
                      </div>
                    )}
                  </div>
                  <div className={"mb-3 col-2"}>
                    <input
                      type="text"
                      placeholder={
                        this.validateCard().card
                          ? this.validateCard().card.code.name
                          : "CVV"
                      }
                      className="form-control"
                      id="cardCvv"
                      onChange={(event) => {
                        this.setState({
                          cardCvv: event.target.value,
                        });
                      }}
                    />
                    {!this.validateExpiry().isPotentiallyValid && (
                      <div id="cardCvvHelp" className="form-text">
                        Please enter a valid{" "}
                        {this.validateCard().card
                          ? this.validateCard().card.code.name
                          : "CVV"}{" "}
                        code.
                      </div>
                    )}
                  </div>
                </div>
                {this.allFieldsValid() ? (
                  <Link to={"/orderSuccessful"}>
                    <button
                      type="submit"
                      className="btn btn-primary"
                      onClick={() => {
                        this.props.clearCart();
                      }}
                    >
                      Submit
                    </button>
                  </Link>
                ) : (
                  <button disabled className={"btn btn-primary"}>
                    Submit
                  </button>
                )}
              </form>
            </div>
          </div>
          <BagAccordion
            key={"AccordianStep2"}
            deliveryAddress={this.props.deliveryAddress}
            updateCart={this.props.updateCart}
            bag={this.props.bag}
          />
        </div>
      </div>
    );
  }
}

export default PaymentBody;
