import React from "react";
import { Link } from "react-router-dom";
import BagAccordion from "./BagAccordion";

function CheckoutBody(props) {
  return (
    <div className={"checkout-body h-100"} style={{ padding: "5rem 10%" }}>
      <div>
        <h2 className={"fw-bolder"}>Account</h2>
        <p>To place your order now, log into your existing account or signup</p>
      </div>
      <div className={"d-flex justify-content-between"}>
        <div className="card" style={{ width: "18rem", height: "19rem" }}>
          <img
            src="https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/auth3.svg"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Checkout as guest?</h5>
            <p className="card-text">
              Offers & Discounts not available when checking out as a guest.
            </p>
            <Link to={'/address'}>
              <button
                className="btn btn-primary fw-bold continue-as-guest-button"
                style={{
                  textDecoration: "none",
                  fontSize: "1rem",
                }}
              >
                Continue as guest
              </button>
            </Link>
          </div>
        </div>
        <BagAccordion key={'AccordianStep1'} updateCart={props.updateCart} bag={props.bag} />
      </div>
    </div>
  );
}

export default CheckoutBody;
