import React from "react";
import BagItemCard from "../Bag/BagItemCard";

const totalPrice = (bag) => {
  return Object.values(bag).reduce((totalPrice, item) => {
    return totalPrice + item.price * item.count;
  }, 0);
};

function BagAccordion(props) {
  return (
    <div key={"BagAccordionContainer"} className={"d-flex flex-column"}>
      <div
        key={"BagAccordion"}
        className="accordion"
        id="bagAccordian"
        style={{ width: "25rem" }}
      >
        <div key={"BagAccordionDetails"} className="accordion-item">
          <h2 className="accordion-header" id="headingOne">
            <button
              className="accordion-button collapsed fw-bold"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#collapseOne"
              aria-expanded="false"
              aria-controls="collapseOne"
            >
              Bag
            </button>
          </h2>
          <div
            id="collapseOne"
            className="accordion-collapse collapse"
            aria-labelledby="headingOne"
            data-bs-parent="#bagAccordian"
          >
            <div
              className="accordion-body pb-0"
              style={{ height: "30rem", overflow: "scroll" }}
            >
              {Object.keys(props.bag).map((item) => {
                return (
                  <div style={{ paddingBottom: "1rem" }}>
                    <BagItemCard
                      updateCart={props.updateCart}
                      key={"bag" + item}
                      item={props.bag[item]}
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        {props.deliveryAddress && (
          <div key={"DeliveryAccordionDetails"} className="accordion-item">
            <h2 className="accordion-header" id="headingThree">
              <button
                className="accordion-button collapsed d-flex"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseThree"
                aria-expanded="false"
                aria-controls="collapseThree"
              >
                <div className={"fw-bold"}>Deliver To</div>
                <div
                  className={"text-muted"}
                  style={{ position: "relative", right: "-9rem" }}
                >
                  {props.deliveryAddress.name}, {props.deliveryAddress.pincode}
                </div>
              </button>
            </h2>
            <div
              id="collapseThree"
              className="accordion-collapse collapse"
              aria-labelledby="headingThree"
              data-bs-parent="#bagAccordian"
            >
              <div className="accordion-body pb-2 text-muted">
                <div>
                  {props.deliveryAddress.addressNumber},{" "}
                  {props.deliveryAddress.address}-
                  {props.deliveryAddress.pincode}
                </div>
                {props.deliveryAddress.phone}
              </div>
            </div>
          </div>
        )}
        <div key={"BagAccordionPrice"} className="accordion-item">
          <h2 className="accordion-header" id="headingTwo">
            <button
              className="accordion-button collapsed d-flex"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#collapseTwo"
              aria-expanded="false"
              aria-controls="collapseTwo"
            >
              <div className={"fw-bold"}>Price Details</div>
              <div
                className={"text-muted"}
                style={{ position: "relative", left: "11rem" }}
              >
                ₹{totalPrice(props.bag).toLocaleString("en-IN")}
              </div>
            </button>
          </h2>
          <div
            id="collapseTwo"
            className="accordion-collapse collapse"
            aria-labelledby="headingTwo"
            data-bs-parent="#bagAccordian"
          >
            <div className="accordion-body py-4 d-flex gap-2 flex-column">
              <div
                className={"d-flex justify-content-between"}
                style={{ fontSize: "0.8rem" }}
              >
                <div>Total Amount</div>
                <div>₹{totalPrice(props.bag).toLocaleString("en-IN")}</div>
              </div>
              <div
                className={"d-flex justify-content-between"}
                style={{ fontSize: "0.8rem" }}
              >
                <div>Shipping</div>
                <div className={"d-flex"}>
                  <s>₹70</s>
                  <div style={{ color: "green" }}>&nbsp;Free</div>
                </div>
              </div>
              <div
                className={"d-flex justify-content-between fs-6 fw-bold"}
                style={{ fontSize: "0.8rem" }}
              >
                <div>You Pay</div>
                <div>₹{totalPrice(props.bag).toLocaleString("en-IN")}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className={"d-flex my-3"}
        style={{
          width: "25rem",
          fontSize: "0.8rem",
          background: "rgb(250 250 250)",
          padding: "0.5rem",
          borderRadius: "0.4rem",
        }}
      >
        <div>
          Buy authentic products. Pay securely. Easy returns and exchange
        </div>
        <img
          src={
            "https://adn-static1.nykaa.com/media/wysiwyg/Payments/desktop-icons/pay-secure-lock.png"
          }
          alt={"..."}
        />
      </div>
    </div>
  );
}

export default BagAccordion;
