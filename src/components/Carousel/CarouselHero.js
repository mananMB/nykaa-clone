import "./CarouselHero.css";

const imageLinks = [
  "https://images-static.nykaa.com/uploads/39e4bef4-72f2-41da-b498-5eb8241d4338.jpg?tr=w-2400,cm-pad_resize",
  "https://images-static.nykaa.com/uploads/d35c472d-62a0-49d0-9d3b-fe96d1878e38.jpg?tr=w-2400,cm-pad_resize",
  "https://images-static.nykaa.com/uploads/83b5d718-ed59-439e-9e26-d287c7e00e37.jpg?tr=w-2400,cm-pad_resize",
  "https://images-static.nykaa.com/uploads/b0390e86-37e8-4d01-a867-104051c7a5f6.jpg?tr=w-2400,cm-pad_resize",
  "https://images-static.nykaa.com/uploads/0fbda79b-61b3-43b8-8bbe-2be9dda4714f.jpg?tr=w-2400,cm-pad_resize",
  "https://images-static.nykaa.com/uploads/aef7d068-0b8f-47f2-b9ab-e49d9ceea68b.jpg?tr=w-2400,cm-pad_resize",
  "https://images-static.nykaa.com/uploads/6bebd1e3-840d-41ab-9fdb-2277acab02ba.jpg?tr=w-2400,cm-pad_resize",
];
const CarouselHero = () => {
  return (
    <div id={"heroCarousel"}>
      <div
        id="heroCarouselIndicators"
        className="carousel slide"
        data-bs-ride="true"
      >
        <div className="carousel-inner">
          {imageLinks.map((link, index) => {
            return (
              <div className={`carousel-item ${index === 1 && "active"}`} key={link+index}>
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={link} className="d-block w-100" alt="hero-image" />
              </div>
            );
          })}
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#heroCarouselIndicators"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#heroCarouselIndicators"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  );
};

export default CarouselHero;
