import React from "react";
import CarouselHero from "../Carousel/CarouselHero.js";
import Products from "../Products/Products";
import allProducts from "../../product-data";

class Homepage extends React.Component {
  // constructor(props) {
  //   super(props);
  // }

  render() {
    return (
      <>
        <CarouselHero />
        <Products products={allProducts} updateCart={this.props.updateCart} />
      </>
    );
  }
}

export default Homepage;
