import React from "react";
import './Footer.css'
const footerItems = [
  "Terms & Conditions",
  "Shipping Policy",
  "Cancellation Policy",
  "Privacy Policy",
];
function Footer(props) {
  return (
    <footer
      className={
        "d-flex align-items-center justify-content-center flex-column py-3 mt-4"
      }
      style={{
        background: "var(--nykaa-pink)",
        fontSize: "0.8rem",
        color: "white",
      }}
    >
      <div className={"footer-links"}>
        {footerItems.map((item) => (
          <div key={item}>{item}</div>
        ))}
      </div>
      <div>© 2022 Nykaa E-Retail Pvt. Ltd. All Rights Reserved.</div>
    </footer>
  );
}

export default Footer;
