import { Link } from "react-router-dom";
import React from "react";

const megaMenuItems = [
  "Makeup",
  "Skin",
  "Hair",
  "Appliances",
  "Bath & Body",
  "Natural",
  "Mom & Baby",
  "Health & Wellness",
  "Men",
  "Fragrance",
  "Pop Ups",
];

const MegaMenu = (props) => {
  return (
    <ul className="nav mega-menu py-1 gap-5 w-100 align-middle justify-content-center">
      {megaMenuItems.map((item) => {
        return (
          <li key={item} className="nav-item">
            <Link className="nav-link active" aria-current="page" to="/">
              {item}
            </Link>
          </li>
        );
      })}
      <li className="nav-item">
        <Link className="nav-link active" aria-current="page" to="/">
          Offers
        </Link>
      </li>
    </ul>
  );
};

export default MegaMenu;
