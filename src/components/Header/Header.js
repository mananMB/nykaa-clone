import React from "react";
import "./Header.css";
import Nav from "./nav.js";
import MegaMenu from "./MegaMenu.js";

class Header extends React.Component {
  render() {
    return (
      <header className={"mb-2"}>
        <Nav
          loggedIn={this.props.loggedIn}
          handleLogout={this.props.handleLogout}
          handleSearchChange={this.props.handleSearchChange}
        />
        <MegaMenu />
      </header>
    );
  }
}

export default Header;
