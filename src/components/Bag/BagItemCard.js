const BagItemCard = (props) => {
  return (
    <div className="card">
      <div className="card-body">
        <div
          className={"d-flex pb-1"}
          style={{
            borderBottom: "1px solid grey",
          }}
        >
          <img
            style={{
              height: "5rem",
            }}
            src={props.item.image}
            className="card-img-left pe-1 w-auto"
            alt="..."
          />
          <p
            className="card-text"
            style={{ fontSize: "1rem", textTransform: "capitalize" }}
          >
            {props.item.title}
          </p>
          <i
            onClick={() => {
              props.updateCart(props.item.id, "remove");
            }}
            className="fa-solid fa-trash"
            style={{
              cursor: "pointer",
              position: "absolute",
              right: "0.8rem",
            }}
          ></i>
        </div>
        <div
          className={"d-flex align-items-center justify-content-between pt-3"}
        >
          <div
            className="btn-group"
            role="group"
            aria-label="Basic mixed styles"
          >
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => {
                if (props.item.count > 1) {
                  props.updateCart(props.item, "removeOne");
                } else {
                  props.updateCart(props.item.id, "remove");
                }
              }}
            >
              -
            </button>
            <div
              className={
                "d-flex align-items-center justify-content-center px-3"
              }
            >
              {props.item.count}
            </div>
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => {
                props.updateCart(props.item);
              }}
            >
              +
            </button>
          </div>
          <div className={'fw-bold'}>₹{(props.item.price * props.item.count).toLocaleString('en-IN')}</div>
        </div>
      </div>
    </div>
  );
};

export default BagItemCard;
