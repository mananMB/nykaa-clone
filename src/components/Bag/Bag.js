import "./Bag.css";
import BagItemCard from "./BagItemCard.js";
import PriceDetailsCard from "./PriceDetailsCard.js";
import { Link } from "react-router-dom";

const totalPrice = (bag) => {
  return Object.values(bag).reduce((totalPrice, item) => {
    return totalPrice + item.price * item.count;
  }, 0);
};

const Bag = (props) => {
  return (
    <>
      <div
        className="offcanvas  offcanvas-end"
        tabIndex="-1"
        id="bag"
        aria-labelledby="bagLabel"
      >
        <div className="offcanvas-header">
          <div
            className={"d-flex justify-content-center gap-3 align-items-center"}
          >
            <i
              className="close-bag fa-solid fa-arrow-left"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></i>
            <h5 className="offcanvas-title fs-3" id="bagLabel">
              Bag
            </h5>
          </div>
        </div>
        <div className="offcanvas-body d-flex flex-column gap-3">
          {Object.keys(props.bag).map((item) => {
            return (
              <BagItemCard
                updateCart={props.updateCart}
                key={"bag" + item}
                item={props.bag[item]}
              />
            );
          })}
          {Object.keys(props.bag).length === 0 ? (
            <div className={"d-flex align-items-center justify-content-center"}>
              <h6>Bag feels empty</h6>
            </div>
          ) : (
            <PriceDetailsCard bag={props.bag} />
          )}
        </div>
        <div
          className="offcanvas-footer"
          style={{ borderTop: "1px solid grey", height: "5rem" }}
        >
          <div
            className={
              "d-flex align-items-center justify-content-between px-4 h-100"
            }
          >
            <h5>₹{totalPrice(props.bag).toLocaleString("en-IN")}</h5>
            {Object.keys(props.bag).length !== 0 ? (
              <Link
                to={"/shoppingBag"}
                style={{ textDecoration: "none", color: "white" }}
              >
                <button
                  className={
                    "btn btn-primary align-items-center d-flex justify-content-between fs-5 fw-bold w-100"
                  }
                  data-bs-dismiss="offcanvas"
                >
                  Proceed
                  <i className="fa-solid fa-arrow-right"></i>
                </button>
              </Link>
            ) : (
              <Link
                to={"/shoppingBag"}
                style={{ textDecoration: "none", color: "white" }}
              >
                <button
                  disabled
                  className={
                    "btn btn-primary align-items-center d-flex justify-content-between fs-5 fw-bold w-100"
                  }
                >
                  Proceed
                  <i className="fa-solid fa-arrow-right"></i>
                </button>
              </Link>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Bag;
