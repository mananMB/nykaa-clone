const totalMrp = (bag) => {
  return Object.values(bag).reduce((totalMrp, item) => {
    return totalMrp + item.mrp * item.count;
  }, 0);
};

const totalPrice = (bag) => {
  return Object.values(bag).reduce((totalPrice, item) => {
    return totalPrice + item.price * item.count;
  }, 0);
};

const PriceDetailsCard = (props) => {
  return (
    <div className="card">
      <div className="card-body">
        <h6 className="card-title">Price Details</h6>
        <div
          className={"d-flex justify-content-between"}
          style={{ fontSize: "0.8rem" }}
        >
          <p>Bag MRP</p>
          <p>₹{(totalMrp(props.bag)).toLocaleString('en-IN')}</p>
        </div>
        <div
          className={"d-flex justify-content-between"}
          style={{ fontSize: "0.8rem" }}
        >
          <p>Bag Discount</p>
          <p>₹{(totalMrp(props.bag) - totalPrice(props.bag)).toLocaleString('en-IN')}</p>
        </div>
        <div className={"d-flex justify-content-between"}>
          <h5>You Pay</h5>
          <h5>₹{(totalPrice(props.bag)).toLocaleString('en-IN')}</h5>
        </div>
      </div>
    </div>
  );
};
export default PriceDetailsCard;
