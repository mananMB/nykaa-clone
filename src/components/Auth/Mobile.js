import React from "react";
import { Link } from "react-router-dom";

class Mobile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      otpFieldValue: "",
      otpFieldError: "",
    };
  }

  handleOtpFieldChange = (event) => {
    this.setState({
      otpFieldValue: event.target.value,
    });
  };

  verifyOTP = (event) => {
    event.preventDefault();
    if (Number(this.state.otpFieldValue) === this.props.OTP) {
      this.props.handleLogin();
    } else {
      this.setState({
        otpFieldError: "PLEASE ENTER VALID OTP",
      });
    }
  };
  render() {
    return (
      <div>
        {/*{(localStorage.loggedIn || false) && <Redirect to={"/"} />}*/}
        <div
          className={
            "d-flex align-items-center justify-content-center min-vh-100 min-vw-100"
          }
          style={{
            background: "rgb(245 245 245)",
            position: "fixed",
            top: 0,
            left: 0,
          }}
        >
          <div
            className="card"
            style={{
              width: "24rem",
              height: "40rem",
              padding: "0.5rem 1.5rem",
              borderRadius: "1rem",
            }}
          >
            <div
              className={
                "d-flex align-items-center justify-content-center py-1"
              }
              style={{ borderBottom: "1px solid silver" }}
            >
              <Link
                to={"/auth"}
                className={"btn btn-close pt-4 position-absolute"}
                style={{ left: "1.3rem" }}
              />
              <h2>Register</h2>
            </div>
            <div
              className="card-body align-items-center"
              style={{ textAlign: "center" }}
            >
              <h6 className="card-subtitle mb-2 py-2 fw-normal">
                Welcome to Nykaa!
              </h6>
              <p
                className="card-text pt-1 text-muted"
                style={{ fontSize: "0.9rem" }}
              >
                Register and earn
                <span style={{ color: "var(--nykaa-pink)" }}>
                  2000 reward points
                </span>
              </p>
              <div className={"mt-4 mb-4"}>
                <form>
                  <fieldset disabled>
                    <input
                      type="text"
                      className="form-control mb-2 text-muted"
                      style={{ textAlign: "center" }}
                      value={this.props.mobileNumber}
                      aria-label="Phone number or Email ID"
                    />
                    <p className={"text-muted"}>
                      Please enter the OTP sent to verify your phone number
                    </p>
                  </fieldset>
                </form>
                <form>
                  <fieldset>
                    <input
                      type="text"
                      className="form-control mb-2"
                      style={{ textAlign: "center" }}
                      value={this.state.otpFieldValue}
                      onChange={this.handleOtpFieldChange}
                      placeholder={"OTP"}
                      aria-label="OTP"
                    />
                    <p
                      style={{
                        color: "red",
                        fontSize: "0.7rem",
                        textAlign: "end",
                        fontWeight: "bold",
                      }}
                    >
                      {this.state.otpFieldError}
                    </p>
                    <p className={"text-muted"}>
                      Please enter the OTP sent to verify your phone number
                    </p>
                    <button
                      className={"btn btn-primary w-100 mt-2"}
                      onClick={this.verifyOTP}
                    >
                      Verify
                    </button>
                  </fieldset>
                </form>
                OTP: {this.props.OTP}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Mobile;
