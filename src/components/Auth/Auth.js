import React, { Component } from "react";
import { Link } from "react-router-dom";
import validator from "validator/es";

const giftSvg = () => {
  return (
    <div className="login-giftBox">
      <svg xmlns="http://www.w3.org/2000/svg" width="296" height="160">
        <title>gift box</title>
        <g fill="none" fillRule="evenodd">
          <ellipse
            cx="148"
            cy="155.5"
            fill="#FFF6FA"
            fillRule="nonzero"
            rx="148"
            ry="4"
          ></ellipse>
          <rect
            width="133"
            height="133"
            x="82"
            y="23"
            fill="#FFF6FA"
            fillRule="nonzero"
            rx="66.5"
          ></rect>
          <path fill="#FF4A90" fillRule="nonzero" d="M112 82h73v74h-73z"></path>
          <path d="M112 82h73v74h-73z"></path>
          <path
            stroke="#C50060"
            strokeWidth="3.83"
            d="M148.2 155.5V82.57s-29.33-22.33-23.41-31.26c10.65-16 23.4 31.26 23.4 31.26s15.18-32.62 20.87-34.12c14.13-3.75 4.57 20.57-20.56 33.55"
          ></path>
          <path fill="#E80071" fillRule="nonzero" d="M163 91h13v6h-13z"></path>
          <path
            stroke="#C50060"
            strokeLinecap="square"
            strokeWidth="3.5"
            d="M114.17 115.78h68.6"
          ></path>
          <circle
            cx="63"
            cy="31.71"
            r="7"
            fill="#E80071"
            fillRule="nonzero"
          ></circle>
          <circle
            cx="232"
            cy="14.71"
            r="7"
            fill="#92E7E0"
            fillRule="nonzero"
          ></circle>
          <path
            fill="#194476"
            fillRule="nonzero"
            d="M149.92.71l7.79 7.81-7.79 7.81-7.78-7.8z"
          ></path>
          <g fillRule="nonzero">
            <path fill="#E80071" d="M205 100v22h9v-17.34z"></path>
            <path
              fill="#001325"
              d="M203 120h13v29.5a6.5 6.5 0 11-13 0 NaNvNaNz"
            ></path>
          </g>
          <circle cx="218" cy="68.72" r="5" stroke="#001325"></circle>
          <circle cx="12" cy="58.72" r="5" stroke="#001325"></circle>
          <path fill="#3063A0" fillRule="nonzero" d="M24 112h59v44H24z"></path>
          <path
            stroke="#194476"
            strokeWidth="3"
            d="M41.67 156v-44.03S19.9 97.32 29.4 97.32c9.5 0 12.27 10.21 12.27 10.21s6.67-15.56 12.44-13.3c5.78 2.25-18.56 24.14-18.56 24.14"
          ></path>
          <path
            stroke="#194476"
            strokeLinecap="square"
            strokeWidth="3"
            d="M25.61 141.78H81.6"
          ></path>
          <path fill="#194476" fillRule="nonzero" d="M55 120h13v6H55z"></path>
          <g>
            <path
              fill="#92E7E0"
              fillRule="nonzero"
              d="M230 115h59v41h-59z"
            ></path>
            <path
              stroke="#59B7B7"
              strokeLinecap="square"
              strokeWidth="3"
              d="M231.5 141.78h56.02"
            ></path>
            <path
              stroke="#59B7B7"
              strokeWidth="3"
              d="M259.68 156v-43.3S237.9 98.28 247.4 98.28s12.28 10.04 12.28 10.04 6.66-15.32 12.43-13.1c5.78 2.22-18.55 23.74-18.55 23.74"
            ></path>
            <path
              fill="#59B7B7"
              fillRule="nonzero"
              d="M268 120h13v6h-13z"
            ></path>
          </g>
        </g>
      </svg>
    </div>
  );
};

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginFieldData: "",
      inputFieldErrorMessage: "REQUIRED",
      disableButton: true,
    };
  }

  handleLoginFieldData = (event) => {
    this.setState({
      loginFieldData: event.target.value,
    });
  };

  validateInputField = () => {
    if (validator.isEmpty(this.state.loginFieldData)) {
      this.setState({
        inputFieldErrorMessage: "REQUIRED",
        disableButton: true,
      });
      return false;
    }
    if (
      validator.isNumeric(this.state.loginFieldData) &&
      !validator.isLength(this.state.loginFieldData, { min: 10, max: 10 })
    ) {
      this.setState({
        inputFieldErrorMessage: "ENTER A VALID MOBILE NUMBER",
        disableButton: true,
      });
      return false;
    }
    if (
      validator.isNumeric(this.state.loginFieldData) &&
      validator.isLength(this.state.loginFieldData, { min: 10, max: 10 })
    ) {
      this.setState({ inputFieldErrorMessage: "", disableButton: false });
      return true;
    }
    if (!validator.isEmail(this.state.loginFieldData)) {
      this.setState({
        inputFieldErrorMessage: "ENTER A VALID EMAIL ADDRESS",
        disableButton: true,
      });
      return false;
    }
    if (validator.isEmail(this.state.loginFieldData)) {
      this.setState({ inputFieldErrorMessage: "", disableButton: false });
      return true;
    }
  };

  handleSubmit = () => {
    const OTP = Math.floor(Math.random() * 1000000);
    this.props.handleLoginFieldData(this.state.loginFieldData, OTP);
  };

  render() {
    return (
      <div
        className={
          "d-flex align-items-center justify-content-center min-vh-100 min-vw-100"
        }
        style={{
          background: "rgb(245 245 245)",
          position: "fixed",
          top: 0,
          left: 0,
        }}
      >
        {/*{(localStorage.loggedIn || false) && <Redirect to={"/"} />}*/}

        <div
          className="card"
          style={{
            width: "24rem",
            height: "40rem",
            padding: "0.5rem 1.5rem",
            borderRadius: "1rem",
          }}
        >
          <Link to={"/"} className={"btn btn-close pt-4 align-self-end"} />
          <div className="card-body">
            <h3 className="card-title">Sign in</h3>
            <h6 className="card-subtitle mb-2 py-2">
              To quickly find your favourites items, saved addresses and
              payments.
            </h6>
            <p
              className="card-text pt-1"
              style={{ borderTop: "1px solid silver" }}
            >
              Register and earn 2000 reward points
            </p>
            {giftSvg()}
            <div className={"mt-4 mb-4"}>
              <input
                type="text"
                className="form-control"
                placeholder="Phone number or Email ID"
                aria-label="Phone number or Email ID"
                value={this.state.loginFieldData}
                onChange={this.handleLoginFieldData}
                onKeyUp={this.validateInputField}
              />
              <div
                className={"mb-2 fw-bold"}
                style={{
                  height: "0.9rem",
                  width: "inherit",
                  textAlign: "end",
                  fontSize: "0.7rem",
                }}
              >
                <small>{this.state.inputFieldErrorMessage}</small>
              </div>
              {this.state.disableButton ? (
                <button disabled className={"btn btn-primary w-100 mt-2"}>
                  Proceed
                </button>
              ) : (
                <Link to={"/auth/mobile"}>
                  <button
                    className={"btn btn-primary w-100 mt-2"}
                    onClick={this.handleSubmit}
                  >
                    Proceed
                  </button>
                </Link>
              )}
            </div>
            <p className={"text-muted"}>
              By continuing, you agree that you have read and accept our T&Cs
              and Privacy Policy.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Auth;
