import SearchSidebar from "./SearchSidebar";
import Products from "../Products/Products";
import "./Search.css";
import allProducts from "../../product-data";
import FuzzySearch from "fuzzy-search";
import { Component } from "react";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortOrder: "Relevance",
    };
  }

  handleSortOrder = (event) => {
    this.setState({
      sortOrder: event.target.value,
    });
  };

  render() {
    const products = new FuzzySearch(allProducts, ["brand", "title"], {
      caseSensitive: false,
    }).search(this.props.query);

    return (
      <div style={{ background: "#f3f3f3" }}>
        <div
          className={"d-flex align-items-center justify-content-center py-5"}
        >
          <img
            className={"w-75"}
            src="https://images-static.nykaa.com/uploads/66350e64-13d1-4347-8842-fc3638b99b38.jpg?tr=w-2400,cm-pad_resize"
            alt={"product-banner"}
          />
        </div>
        <div
          style={{
            textAlign: "center",
            paddingBottom: "2rem",
            textTransform: "capitalize",
          }}
        >
          <h1>Showing results for {this.props.query}</h1>
        </div>
        <div className={"d-flex justify-content-center flex-wrap"}>
          <SearchSidebar
            products={products}
            sortOrder={this.state.sortOrder}
            handleSortOrder={this.handleSortOrder}
          />
          <Products
            sortOrder={this.state.sortOrder}
            products={products}
            updateCart={this.props.updateCart}
            width={"60%"}
          />
        </div>
      </div>
    );
  }
}

export default Search;
