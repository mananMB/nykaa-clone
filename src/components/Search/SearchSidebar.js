import React from "react";

function SearchSidebar(props) {
  return (
    <div className={"filters-sidebar pb-3"} style={{ minWidth: " 20rem" }}>
      <div className="accordion" id="sortAccordian">
        <div className="accordion-item">
          <h2 className="accordion-header" id="headingTwo">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#collapseTwo"
              aria-expanded="false"
              aria-controls="collapseTwo"
            >
              <div className={"fw-bold"}>Sort By:&nbsp;{props.sortOrder}</div>
            </button>
          </h2>
          <div
            id="collapseTwo"
            className="accordion-collapse collapse"
            aria-labelledby="headingTwo"
            data-bs-parent="#sortAccordian"
          >
            <div className="accordion-body">
              <div className="accordion-body">
                <form>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadio"
                      id="flexRadioDefault1"
                      value={"Relevance"}
                      defaultChecked
                      onChange={props.handleSortOrder}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault1"
                    >
                      Relevance
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadio"
                      id="flexRadioDefault2"
                      value={"Price: High to Low"}
                      onChange={props.handleSortOrder}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault2"
                    >
                      Price: High to Low
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadio"
                      id="flexRadioDefault3"
                      value={"Price: Low to High"}
                      onChange={props.handleSortOrder}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault3"
                    >
                      Price: Low to High
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadio"
                      id="flexRadioDefault4"
                      value={"Name"}
                      onChange={props.handleSortOrder}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault4"
                    >
                      Name
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadio"
                      id="flexRadioDefault5"
                      value={"Discount"}
                      onChange={props.handleSortOrder}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault5"
                    >
                      Discount
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadio"
                      id="flexRadioDefault6"
                      value={"Customer Top Rated"}
                      onChange={props.handleSortOrder}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault6"
                    >
                      Customer Top Rated
                    </label>
                  </div>
                  {/*<div className="form-check">*/}
                  {/*  <label*/}
                  {/*    className="form-check-label"*/}
                  {/*    htmlFor="flexRadioDefault1"*/}
                  {/*  >*/}
                  {/*    Relevance*/}
                  {/*  </label>*/}
                  {/*  <input*/}
                  {/*    className="form-check-input"*/}
                  {/*    type="radio"*/}
                  {/*    name="flexRadioDefault"*/}
                  {/*    id="flexRadioDefault1"*/}
                  {/*  />*/}
                  {/*</div>*/}
                  {/*<div className="form-check">*/}
                  {/*  <label*/}
                  {/*    className="form-check-label"*/}
                  {/*    htmlFor="flexRadioDefault2"*/}
                  {/*  >*/}
                  {/*    Price: High to Low*/}
                  {/*  </label>*/}
                  {/*  <input*/}
                  {/*    className="form-check-input"*/}
                  {/*    type="radio"*/}
                  {/*    name="flexRadioDefault"*/}
                  {/*    id="flexRadioDefault2"*/}
                  {/*  />*/}
                  {/*</div>*/}
                  {/*<div className="form-check">*/}
                  {/*  <label*/}
                  {/*    className="form-check-label"*/}
                  {/*    htmlFor="flexRadioDefault2"*/}
                  {/*  >*/}
                  {/*    Price: Low to High*/}
                  {/*  </label>*/}
                  {/*  <input*/}
                  {/*    className="form-check-input"*/}
                  {/*    type="radio"*/}
                  {/*    name="flexRadioDefault"*/}
                  {/*    id="flexRadioDefault2"*/}
                  {/*  />*/}
                  {/*</div>*/}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SearchSidebar;
