import "./App.css";
import { Link, Redirect, Route, Switch } from "react-router-dom";
import Homepage from "./components/Homepage/Homepage.js";
import Bag from "./components/Bag/Bag.js";
import React from "react";
import Auth from "./components/Auth/Auth.js";
import Search from "./components/Search/Search.js";
import Header from "./components/Header/Header.js";
import Mobile from "./components/Auth/Mobile.js";
import Footer from "./components/Footer/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Product from "./components/Product/Product";
import Checkout from "./components/Checkout/Checkout";
import Address from "./components/Checkout/Address";
import Payment from "./components/Checkout/Payment";
import OrderCreatedPage from "./components/Checkout/OrderCreatedPage";

function titleCase(str) {
  str = str
    .toLowerCase()
    .split(" ")
    .map(function (word) {
      return word.replace(word[0], word[0].toUpperCase());
    });
  return str.join(" ");
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      loginFieldData: "",
      bag: (localStorage.bagItems && JSON.parse(localStorage.bagItems)) || {},
      address: (localStorage.address && JSON.parse(localStorage.address)) || [],
      deliveryAddress: "",
    };
  }

  syncCartToLocalStorage = () => {
    localStorage.bagItems = JSON.stringify(this.state.bag);
  };

  updateAddress = (address) => {
    this.setState(
      (state) => {
        return {
          address: [...state.address, address],
        };
      },
      () => {
        localStorage.address = JSON.stringify(this.state.address);
      }
    );
  };

  updateDeliveryAddress = (address) => {
    this.setState((state) => {
      return {
        deliveryAddress: address,
      };
    });
  };

  clearCart = () => {
    this.setState(
      () => {
        return { bag: {} };
      },
      () => {
        // toast(`Cart cleared.`);
        this.syncCartToLocalStorage();
      }
    );
  };

  updateCart = (product, action) => {
    const { id, image, title, mrp, price } = product;
    const bagItem = { id, image, title, mrp, price };

    switch (action) {
      case "remove":
        const productTitle = this.state.bag[product].title;
        this.setState(
          (state) => {
            const bagCopy = state.bag;
            delete bagCopy[product];
            return {
              bag: { ...bagCopy },
            };
          },
          () => {
            toast(`${titleCase(productTitle)} removed from bag.`);
            this.syncCartToLocalStorage();
          }
        );
        break;
      case "removeOne":
        this.setState(
          (state) => {
            if (state.bag[product.id].count > 1) {
              return {
                bag: {
                  ...state.bag,
                  [product.id]: {
                    ...bagItem,
                    count: state.bag[product.id].count - 1,
                    totalItemCost:
                      state.bag[product.id].totalItemCost -
                      parseFloat(product.price),
                  },
                },
              };
            }
            this.updateCart(product, "remove");
          },
          () => {
            toast(`${titleCase(product.title)} quantity lowered by one.`);
            this.syncCartToLocalStorage();
          }
        );
        break;
      //default action is add to bag
      default:
        this.setState(
          (state) => {
            if (!state.bag[product.id]) {
              return {
                bag: {
                  ...state.bag,
                  [product.id]: {
                    ...bagItem,
                    count: 1,
                    totalItemCost: parseFloat(product.price),
                  },
                },
              };
            }
            return {
              bag: {
                ...state.bag,
                [product.id]: {
                  ...bagItem,
                  count: state.bag[product.id].count + 1,
                  totalItemCost:
                    state.bag[product.id].totalItemCost +
                    parseFloat(product.price),
                },
              },
            };
          },
          () => {
            toast(`${titleCase(product.title)} added to bag.`);
            this.syncCartToLocalStorage();
          }
        );
    }
  };

  handleLoginFieldData = (loginDetails, OTP) => {
    this.setState({
      loginFieldData: loginDetails,
      OTP: OTP,
    });
  };

  handleLogin = () => {
    this.setState(
      {
        loggedIn: true,
      },
      () => {
        localStorage.loggedIn = true;
      }
    );
  };

  handleLogout = () => {
    this.setState(
      {
        loggedIn: false,
      },
      () => {
        localStorage.loggedIn = false;
      }
    );
  };

  // handleSearchChange = (query) => {
  //   console.log(query)
  //   this.setState({
  //     query: query
  //   })
  // }
  render() {
    return (
      <div className="App">
        <Header
          loggedIn={this.state.loggedIn}
          handleLogout={this.handleLogout}
          handleSearchChange={this.handleSearchChange}
        />
        <Switch>
          <Route
            exact
            path="/"
            render={(props) => <Homepage updateCart={this.updateCart} />}
          />
          <Route
            exact
            path={"/search/:query"}
            render={(props) => (
              <Search
                updateCart={this.updateCart}
                query={props.match.params.query}
              />
            )}
          />
          <Route
            exact
            path="/auth"
            render={(props) => (
              <Auth handleLoginFieldData={this.handleLoginFieldData} />
            )}
          />
          <Route
            path="/product/:id"
            render={(props) => (
              <Product
                bag={this.state.bag}
                updateCart={this.updateCart}
                id={props.match.params.id}
              />
            )}
          />
          <Route
            path={"/shoppingBag"}
            render={() => (
              <Checkout updateCart={this.updateCart} bag={this.state.bag} />
            )}
          />
          <Route
            path={"/address"}
            render={() => (
              <Address
                address={this.state.address}
                updateCart={this.updateCart}
                bag={this.state.bag}
                updateDeliveryAddress={this.updateDeliveryAddress}
                updateAddress={this.updateAddress}
              />
            )}
          />
          <Route
            path={"/payment"}
            render={() => (
              <Payment
                address={this.state.address}
                updateCart={this.updateCart}
                bag={this.state.bag}
                updateAddress={this.updateAddress}
                clearCart={this.clearCart}
                deliveryAddress={this.state.deliveryAddress}
              />
            )}
          />
          <Route
            path={"/orderSuccessful"}
            render={() => <OrderCreatedPage bag={this.state.bag} />}
          />
          {!this.state.loggedIn ? (
            <Route
              exact
              path="/auth"
              render={(props) => (
                <Auth handleLoginFieldData={this.handleLoginFieldData} />
              )}
            />
          ) : (
            <Redirect to="/" />
          )}
          {!this.state.loggedIn ? (
            <Route
              path={"/auth/mobile"}
              render={(props) => (
                <Mobile
                  mobileNumber={this.state.loginFieldData}
                  handleLogin={this.handleLogin}
                  OTP={this.state.OTP}
                />
              )}
            />
          ) : (
            <Redirect to="/" />
          )}
        </Switch>
        <Bag updateCart={this.updateCart} bag={this.state.bag} />
        <Footer />
        <ToastContainer
          position="bottom-left"
          autoClose={2000}
          hideProgressBar={true}
          newestOnTop={true}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          pauseOnHover
          theme="light"
        />
      </div>
    );
  }
}

export default App;
