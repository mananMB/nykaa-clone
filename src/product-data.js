const products = [
  {
    id: "6375e0963173b5a37aa7064c",
    title: "cillum mollit commodo",
    description:
      "Do irure est tempor in in excepteur sunt labore deserunt dolor. Veniam quis sint adipisicing excepteur et. Deserunt eiusmod aliqua anim occaecat commodo irure exercitation enim magna laboris ullamco id in. Amet in voluptate consequat laboris commodo mollit. Voluptate proident aliqua labore ullamco ea ex. Exercitation qui dolore ullamco labore consectetur sit nisi enim.",
    rating: {
      rate: 3.94,
      count: 100,
    },
    discount: 15,
    mrp: 8278,
    category: "skin",
    price: "7036",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/a/a/aaa63f68904245707316_rev1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e0960c09ef703c739329",
    title: "anim ipsum dolor",
    description:
      "Dolore aute cupidatat sunt enim ullamco. Eu dolor eu incididunt proident irure reprehenderit duis magna laborum. Voluptate commodo in qui deserunt aute ipsum anim ad. Laboris pariatur cillum quis amet exercitation nulla ipsum qui eu. Quis proident aliqua nostrud commodo non duis adipisicing cillum esse esse elit proident dolor qui. Occaecat occaecat qui sunt quis non cillum occaecat ea consequat.",
    rating: {
      rate: 3.21,
      count: 379,
    },
    discount: 15,
    mrp: 4044,
    category: "appliances",
    price: "3437",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711924_1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e096f756ea0fb0ebca54",
    title: "nulla id pariatur",
    description:
      "Duis adipisicing laborum qui id voluptate nulla consequat excepteur voluptate proident sunt ut velit. Excepteur anim veniam labore laborum veniam in nostrud excepteur deserunt. Enim cupidatat nulla sit laboris amet adipisicing duis. Mollit dolore enim sit Lorem. Magna ad anim cillum aliquip. Est dolor dolor sunt fugiat.",
    rating: {
      rate: 3.61,
      count: 331,
    },
    discount: 5,
    mrp: 783,
    category: "skin",
    price: "744",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e0967197ff63133708fe",
    title: "fugiat nostrud eiusmod",
    description:
      "Ipsum sit sunt dolor occaecat nulla qui ad elit. Consectetur cupidatat ex occaecat ullamco dolore dolor pariatur proident veniam incididunt. Duis eu consequat sunt reprehenderit. Consequat commodo consectetur incididunt occaecat esse ipsum. Ullamco occaecat tempor et cupidatat minim incididunt culpa amet et est amet. Elit magna nisi excepteur Lorem occaecat adipisicing mollit consectetur elit.",
    rating: {
      rate: 1.14,
      count: 514,
    },
    discount: 20,
    mrp: 3622,
    category: "hair",
    price: "2898",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/f/a/face-primer.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e0966e1081ffc51578e9",
    title: "dolor et est",
    description:
      "Voluptate laboris deserunt ipsum esse sint aliquip quis dolore deserunt esse cillum anim fugiat labore. Ea nisi quis eu nisi exercitation. Nulla officia consequat esse magna voluptate veniam commodo. Ex veniam exercitation exercitation elit mollit Lorem minim excepteur ut eiusmod ad proident amet consectetur. Culpa amet pariatur duis do aliquip incididunt laboris irure. Ad deserunt irure culpa incididunt sit officia occaecat exercitation nisi eu mollit.",
    rating: {
      rate: 2.83,
      count: 816,
    },
    discount: 10,
    mrp: 3314,
    category: "makeup",
    price: "2983",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e0960db8ea7ff8d45d05",
    title: "minim proident quis",
    description:
      "Sint cillum mollit aliqua ea voluptate deserunt eiusmod non eiusmod occaecat in esse proident. Culpa aliqua Lorem magna id in amet deserunt. Eiusmod Lorem excepteur consequat ullamco excepteur enim laborum incididunt qui fugiat nostrud consectetur irure occaecat. Cupidatat ad eu velit aute ea in id sit magna pariatur mollit cupidatat. Enim nisi ad exercitation nulla reprehenderit tempor laboris pariatur incididunt officia cupidatat esse id. Ex est Lorem esse nulla ullamco consequat velit nostrud magna laborum sunt non ut sit.",
    rating: {
      rate: 3.49,
      count: 364,
    },
    discount: 10,
    mrp: 8207,
    category: "natural",
    price: "7386",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/3/13967ab8904245700454_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e096693b86ae84ce6cf7",
    title: "velit adipisicing et",
    description:
      "Nulla nisi incididunt sunt dolore est incididunt. Laboris deserunt sunt nulla magna irure magna cillum mollit quis duis elit laboris. Non amet cillum exercitation magna reprehenderit quis elit dolore. Esse eiusmod consequat exercitation quis aute cupidatat amet ea consequat proident aliqua amet magna. Aliqua do fugiat veniam elit officia incididunt cupidatat irure esse qui irure mollit. Sit exercitation velit aliqua sint eu minim quis amet exercitation amet ut sint do labore.",
    rating: {
      rate: 1.22,
      count: 581,
    },
    discount: 20,
    mrp: 2609,
    category: "hair",
    price: "2087",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e09680f1b9fc2f1619f5",
    title: "do mollit exercitation",
    description:
      "Consequat exercitation sit ipsum duis quis dolor cillum et eiusmod commodo aute amet mollit occaecat. Ex eu sunt id consectetur velit et deserunt. Cillum sint amet sit non quis anim. Irure eiusmod minim culpa dolore amet ea in occaecat duis tempor nulla ea ex ex. Laborum incididunt eiusmod nostrud ex consequat proident cupidatat. Culpa minim eu adipisicing irure veniam eiusmod laborum do.",
    rating: {
      rate: 1.06,
      count: 191,
    },
    discount: 10,
    mrp: 4496,
    category: "makeup",
    price: "4046",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e096c8878a1b9318b7c3",
    title: "esse ad est",
    description:
      "Ipsum ad nostrud excepteur laboris elit exercitation cillum voluptate dolore id excepteur occaecat quis voluptate. Deserunt aliquip enim non anim tempor nisi incididunt dolore id aliqua ex. Tempor reprehenderit in in eiusmod. Consectetur laborum occaecat Lorem et anim ut duis. Duis enim culpa ad ea ea ex elit eiusmod commodo enim reprehenderit ullamco do. Tempor pariatur proident commodo pariatur do ullamco aliquip officia enim aute mollit elit.",
    rating: {
      rate: 3.33,
      count: 434,
    },
    discount: 15,
    mrp: 5112,
    category: "hair",
    price: "4345",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Innisfree",
  },
  {
    id: "6375e096e3fb3585592c360a",
    title: "ut deserunt cillum",
    description:
      "Officia nulla ea aute proident sint deserunt laboris et consequat aute. Duis exercitation commodo aute ipsum eiusmod quis fugiat eu ut. Duis adipisicing dolore commodo duis. Quis quis exercitation eu esse. Veniam aute nostrud mollit cillum cupidatat consequat ut id dolore tempor nostrud. Et ullamco do aliqua est commodo minim tempor adipisicing pariatur.",
    rating: {
      rate: 1.87,
      count: 861,
    },
    discount: 20,
    mrp: 7622,
    category: "appliances",
    price: "6098",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e09627ad8d155ed708f9",
    title: "deserunt anim non",
    description:
      "Mollit minim incididunt tempor laboris ullamco minim non in commodo reprehenderit sunt ullamco ullamco nisi. Lorem nulla ut velit culpa eu occaecat velit non pariatur. Magna cillum dolor reprehenderit aute. Amet ad exercitation laborum ipsum est incididunt id culpa minim adipisicing reprehenderit adipisicing excepteur. Elit deserunt velit sint deserunt id. Dolor irure dolor ea ut officia anim commodo exercitation est ex pariatur.",
    rating: {
      rate: 4.93,
      count: 813,
    },
    discount: 15,
    mrp: 1306,
    category: "appliances",
    price: "1110",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e0968435b7fb70f047ba",
    title: "culpa minim pariatur",
    description:
      "Dolor dolor excepteur ad ea velit. Do amet cupidatat minim labore incididunt pariatur non et dolore laboris. Officia id dolore pariatur aliqua dolore esse irure consequat irure reprehenderit. Nisi commodo ullamco elit velit consectetur amet consectetur voluptate labore nostrud reprehenderit fugiat Lorem commodo. Aliqua anim qui cupidatat esse reprehenderit non excepteur irure aliquip. In et commodo voluptate fugiat.",
    rating: {
      rate: 3.25,
      count: 275,
    },
    discount: 15,
    mrp: 6233,
    category: "makeup",
    price: "5298",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "The Face Shop",
  },
  {
    id: "6375e09670efd5188f0289cb",
    title: "aliquip exercitation deserunt",
    description:
      "Nulla consequat est aliquip cillum. Cupidatat ipsum culpa veniam exercitation velit dolor labore sunt. Commodo magna aute nulla ex esse et cupidatat occaecat magna enim ex in. Eu id ea velit velit. Sit aliqua culpa quis labore dolor velit amet sunt id cillum sint sint. Labore fugiat do aliqua laborum.",
    rating: {
      rate: 3.85,
      count: 515,
    },
    discount: 5,
    mrp: 8018,
    category: "appliances",
    price: "7617",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e09638c4649093ad8623",
    title: "velit enim aliquip",
    description:
      "Eu ullamco incididunt ut incididunt ut eiusmod magna. Irure ipsum enim tempor reprehenderit non aliquip anim consequat. Tempor veniam proident deserunt sunt Lorem duis ad veniam cillum sit. Pariatur ex amet dolore aute labore do non culpa pariatur amet excepteur aute dolor. Mollit laborum consectetur magna dolor nulla incididunt irure adipisicing incididunt in dolor id consectetur. Velit culpa duis nisi veniam consequat tempor commodo amet dolore sit.",
    rating: {
      rate: 3.77,
      count: 376,
    },
    discount: 15,
    mrp: 2742,
    category: "appliances",
    price: "2331",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/3/13967ab8904245700454_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e0966459c4db16da7140",
    title: "magna cillum sint",
    description:
      "Esse esse est amet sit enim. Eu mollit consectetur ipsum cillum in. Sit magna adipisicing exercitation anim. Eu sit occaecat dolor enim do. Esse enim sit nulla adipisicing ipsum. Magna magna occaecat mollit dolore aliquip sunt tempor in sunt in mollit excepteur.",
    rating: {
      rate: 3.58,
      count: 217,
    },
    discount: 15,
    mrp: 2910,
    category: "hair",
    price: "2474",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/b/4/b4d615aNYKAC00000424n_1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e096ad2a0080c21982b6",
    title: "incididunt eiusmod nulla",
    description:
      "Aute eu velit irure proident laborum eiusmod nulla id sunt et. Ut incididunt nostrud consequat incididunt ad proident enim adipisicing occaecat eiusmod magna anim quis Lorem. Reprehenderit fugiat elit sint est do aliqua adipisicing nisi amet amet. Ullamco Lorem eiusmod in sint quis. Eu esse ad mollit consequat veniam labore do quis laboris ex elit dolor aliqua. Cillum minim culpa aliqua pariatur minim aliqua veniam.",
    rating: {
      rate: 2.66,
      count: 999,
    },
    discount: 15,
    mrp: 978,
    category: "makeup",
    price: "831",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "Biotique",
  },
  {
    id: "6375e09658c38565ae0c2dc9",
    title: "occaecat in est",
    description:
      "Exercitation elit labore fugiat exercitation occaecat. Do qui ea ullamco cillum. Qui consectetur cupidatat fugiat ea reprehenderit ullamco excepteur adipisicing elit. Reprehenderit reprehenderit consectetur minim est nostrud ea occaecat eu dolore. Reprehenderit labore eu cupidatat reprehenderit eiusmod in nulla in voluptate sunt. Do consequat adipisicing elit eu laborum duis Lorem consectetur ad fugiat dolore dolor.",
    rating: {
      rate: 3.71,
      count: 387,
    },
    discount: 10,
    mrp: 4204,
    category: "makeup",
    price: "3784",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "The Face Shop",
  },
  {
    id: "6375e0965aee7e1088cb0c8e",
    title: "reprehenderit exercitation cillum",
    description:
      "Officia duis tempor consectetur do voluptate occaecat cupidatat qui tempor. Id id consectetur non laboris adipisicing eu eiusmod id et ullamco laborum laboris sunt. Ipsum incididunt ad sint fugiat minim. Deserunt nulla laborum nulla culpa. Aute elit aliqua amet id dolore sint amet. Quis duis ad veniam veniam.",
    rating: {
      rate: 3.99,
      count: 611,
    },
    discount: 5,
    mrp: 8087,
    category: "natural",
    price: "7683",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/a/a/aaa63f68904245707316_rev1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e096a8449bd3ef20d7d5",
    title: "consequat quis magna",
    description:
      "Elit duis ipsum nisi consequat ut. Nulla duis anim et irure do sunt aute culpa ad sit. Tempor elit aliqua non quis eu ipsum cillum eu ut veniam aute. Commodo amet enim ad minim incididunt ea. Consequat duis in Lorem do Lorem nisi sit. Irure aliquip nulla aliqua esse reprehenderit laborum adipisicing.",
    rating: {
      rate: 4.57,
      count: 902,
    },
    discount: 20,
    mrp: 5457,
    category: "skin",
    price: "4366",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e09628e08510e828f3a3",
    title: "dolore exercitation fugiat",
    description:
      "Officia anim enim do proident deserunt ea sit proident enim incididunt nisi ullamco non deserunt. Sunt fugiat eu cillum excepteur consequat ea. Eiusmod fugiat deserunt proident ea nulla incididunt duis culpa elit do tempor fugiat aliquip. Quis magna ad ipsum pariatur nulla cupidatat aliquip aliqua proident dolor eu enim ut pariatur. Officia ex veniam do deserunt Lorem mollit Lorem nostrud ea culpa. Ipsum duis ad officia commodo ipsum eiusmod mollit ad irure ad laboris pariatur aliqua Lorem.",
    rating: {
      rate: 1.43,
      count: 705,
    },
    discount: 5,
    mrp: 2031,
    category: "appliances",
    price: "1929",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "The Face Shop",
  },
  {
    id: "6375e096b00adc6172b81410",
    title: "ea enim qui",
    description:
      "Irure elit nostrud nostrud quis sunt esse aute. Ea duis voluptate incididunt enim. Magna ullamco quis ullamco ullamco. Dolore magna ex ut dolore do cupidatat cillum excepteur nisi consequat ipsum commodo consectetur. Irure et do cupidatat ea eiusmod excepteur duis elit tempor dolore. Commodo laboris deserunt fugiat occaecat eiusmod irure non laboris elit eiusmod.",
    rating: {
      rate: 3.67,
      count: 246,
    },
    discount: 5,
    mrp: 8341,
    category: "natural",
    price: "7924",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e0962b97216ecaa8fea5",
    title: "proident et Lorem",
    description:
      "Quis eu sit nulla est Lorem quis ullamco magna sunt consectetur dolor ipsum. Tempor consectetur laboris do mollit officia in enim veniam Lorem incididunt. Dolore irure incididunt fugiat duis exercitation veniam excepteur consequat. Excepteur velit ea esse duis nulla elit est deserunt reprehenderit duis. Fugiat proident do anim nostrud pariatur duis et aliqua aliqua magna irure duis. Amet mollit adipisicing id irure nulla dolor irure reprehenderit.",
    rating: {
      rate: 2.46,
      count: 381,
    },
    discount: 10,
    mrp: 5279,
    category: "skin",
    price: "4751",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/f/a/face-primer.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096841131e03c48f9ba",
    title: "reprehenderit et dolor",
    description:
      "Deserunt irure non aute veniam proident consequat velit ut adipisicing commodo aliquip Lorem consectetur est. Laboris cillum duis pariatur ipsum mollit Lorem ullamco anim eiusmod labore eiusmod sunt enim. Minim aliqua adipisicing aliqua incididunt labore aute elit. Laborum sint reprehenderit pariatur ipsum ullamco incididunt pariatur officia non proident. Deserunt ex id est sunt consequat sunt deserunt. Fugiat do non tempor exercitation commodo qui.",
    rating: {
      rate: 4.24,
      count: 128,
    },
    discount: 5,
    mrp: 9117,
    category: "appliances",
    price: "8661",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e09646a0bdb39fbd8546",
    title: "officia Lorem sint",
    description:
      "Dolore sint velit deserunt sit occaecat. Adipisicing excepteur nulla excepteur in Lorem dolor. Quis cupidatat nulla aute consequat. Nostrud culpa minim esse commodo incididunt esse duis sint ad ea laborum consectetur aliqua. Adipisicing duis Lorem excepteur deserunt nostrud Lorem tempor fugiat amet laborum non. Consectetur ea aute do ex exercitation magna ipsum adipisicing sunt.",
    rating: {
      rate: 3.26,
      count: 202,
    },
    discount: 20,
    mrp: 8715,
    category: "skin",
    price: "6972",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e096fccf9cc0046cc769",
    title: "fugiat occaecat sunt",
    description:
      "Excepteur voluptate veniam deserunt ea quis incididunt mollit ipsum cupidatat elit. Occaecat officia consequat irure pariatur exercitation minim in commodo nostrud esse cillum et. Exercitation minim eu incididunt cillum laborum ullamco. Minim aliqua proident sunt ipsum duis eiusmod dolor non deserunt do officia tempor culpa laboris. Deserunt qui ex proident ad in exercitation labore culpa dolor incididunt. Occaecat sit cupidatat elit nulla cillum irure sunt commodo cillum tempor reprehenderit ullamco.",
    rating: {
      rate: 2.21,
      count: 700,
    },
    discount: 5,
    mrp: 2749,
    category: "natural",
    price: "2612",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e096276d5bad97278c4f",
    title: "exercitation ipsum do",
    description:
      "Amet ipsum consectetur proident deserunt mollit ullamco reprehenderit nostrud laboris laborum exercitation adipisicing laboris enim. Mollit aliqua do do ad sunt reprehenderit culpa ut dolor ipsum. Reprehenderit pariatur aliqua duis reprehenderit reprehenderit enim qui do aute anim. Culpa magna ex excepteur irure ad. Magna ex qui sit ea ex qui veniam ullamco occaecat consequat est. Velit ipsum enim irure aliqua velit deserunt id dolore mollit laboris enim nulla laboris ex.",
    rating: {
      rate: 2.64,
      count: 462,
    },
    discount: 10,
    mrp: 3783,
    category: "natural",
    price: "3405",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e09635686c4fd0f98eee",
    title: "labore culpa laboris",
    description:
      "Labore enim magna cupidatat enim labore adipisicing anim laboris ex est labore eu occaecat culpa. Nisi veniam cupidatat velit ea nostrud enim ea dolor. Nostrud ut ex exercitation nisi. Lorem quis do voluptate cillum enim culpa exercitation cupidatat dolor labore qui deserunt. Adipisicing proident excepteur aute nisi tempor. Officia velit cillum cupidatat ad sit aliquip veniam commodo deserunt et.",
    rating: {
      rate: 2.99,
      count: 739,
    },
    discount: 10,
    mrp: 6768,
    category: "makeup",
    price: "6091",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096d8d12067e2d30a5d",
    title: "amet mollit in",
    description:
      "Et esse minim mollit do cillum. Do nostrud eu dolor est nisi ipsum in consequat quis consequat commodo. Amet quis consectetur ut aute anim consectetur. Enim occaecat culpa nisi sunt magna dolor amet nostrud sint nulla et. Non sint ea qui consectetur laborum dolor non ex incididunt tempor do veniam proident. Dolore voluptate cillum Lorem ut est.",
    rating: {
      rate: 2.21,
      count: 917,
    },
    discount: 10,
    mrp: 3726,
    category: "natural",
    price: "3353",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e096774be6873f9dbf86",
    title: "culpa quis est",
    description:
      "Incididunt adipisicing magna nisi tempor anim. Fugiat culpa do id mollit consequat ipsum quis mollit mollit. Laboris in consequat do velit et occaecat adipisicing officia ad magna proident. Quis exercitation est commodo aliqua amet ullamco culpa nulla eiusmod exercitation pariatur ullamco incididunt quis. Sunt sunt anim commodo do nulla deserunt irure duis aliquip adipisicing pariatur ipsum velit. Occaecat dolore nisi in nulla elit veniam aliqua sit exercitation.",
    rating: {
      rate: 3.42,
      count: 534,
    },
    discount: 5,
    mrp: 1232,
    category: "appliances",
    price: "1170",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/a/a/aaa63f68904245707316_rev1.jpg",
    brand: "The Face Shop",
  },
  {
    id: "6375e096658a38e385318895",
    title: "minim non consequat",
    description:
      "Enim exercitation ut voluptate ipsum duis officia deserunt minim sint dolor culpa qui esse tempor. Ut consequat sint exercitation id magna consequat sint amet esse aute eiusmod fugiat. Mollit fugiat incididunt dolore reprehenderit voluptate duis ut sunt dolor. Qui adipisicing ex mollit laborum Lorem pariatur veniam labore occaecat consequat irure irure nisi. Sunt sint cupidatat Lorem labore ullamco magna. Cupidatat sunt ut ullamco esse officia commodo occaecat amet mollit consectetur commodo fugiat aute.",
    rating: {
      rate: 4.26,
      count: 662,
    },
    discount: 5,
    mrp: 4522,
    category: "appliances",
    price: "4296",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/3/13967ab8904245700454_1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e0961a92992a97bef245",
    title: "nisi anim Lorem",
    description:
      "Quis sit Lorem nisi laboris consequat est dolor laborum minim velit aute consectetur. Magna ea ex exercitation ullamco duis proident commodo culpa. Esse ex deserunt commodo excepteur nisi ut velit nisi sunt eiusmod. Pariatur laborum incididunt commodo nisi esse ullamco laboris sunt labore est. Sunt officia fugiat voluptate consequat nulla enim anim dolor incididunt do. Irure consectetur minim ea magna officia irure minim et veniam pariatur.",
    rating: {
      rate: 4.69,
      count: 458,
    },
    discount: 10,
    mrp: 2901,
    category: "makeup",
    price: "2611",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e096889682feffede96e",
    title: "nulla incididunt mollit",
    description:
      "Pariatur dolore cupidatat cupidatat ut culpa reprehenderit et. Deserunt do ipsum exercitation ut nisi minim velit enim sunt. Anim sunt commodo sint quis consectetur ad aute consectetur sunt anim Lorem nisi pariatur. Pariatur velit sit laboris laborum. Culpa duis minim officia laborum cillum non aliqua esse quis ullamco qui excepteur velit. Fugiat ut cupidatat consectetur elit reprehenderit exercitation qui in anim aliquip nisi sunt.",
    rating: {
      rate: 3.55,
      count: 432,
    },
    discount: 10,
    mrp: 3461,
    category: "hair",
    price: "3115",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e096653de1ba7d0013a7",
    title: "ex culpa aliquip",
    description:
      "Ut eiusmod labore mollit exercitation nostrud do qui. Exercitation irure do reprehenderit laboris deserunt incididunt do consequat nostrud non. Excepteur aute fugiat exercitation anim reprehenderit est pariatur irure. Irure aute ea ullamco sit. Reprehenderit labore labore ex ex laborum occaecat ullamco ad in sit excepteur. Qui enim nostrud consequat duis exercitation amet.",
    rating: {
      rate: 2.06,
      count: 513,
    },
    discount: 20,
    mrp: 9305,
    category: "natural",
    price: "7444",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/b/4/b4d615aNYKAC00000424n_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096d66a8f989551f6e1",
    title: "voluptate esse aliqua",
    description:
      "Aliqua magna ut magna consequat velit cupidatat veniam. Nisi amet duis cillum elit. Aliquip sint dolore irure aute sit consectetur sunt. Elit ex est Lorem in ullamco dolore Lorem commodo. Laboris tempor adipisicing ea quis reprehenderit id duis amet. Officia sunt adipisicing culpa non velit officia.",
    rating: {
      rate: 4.78,
      count: 383,
    },
    discount: 15,
    mrp: 1222,
    category: "skin",
    price: "1039",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/a/a/aaa63f68904245707316_rev1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e0966499d0ae59615abd",
    title: "velit voluptate officia",
    description:
      "Est enim irure in labore adipisicing tempor Lorem irure incididunt minim commodo veniam quis. Consequat consectetur ex et est amet dolore velit non ut. Proident pariatur proident culpa Lorem labore. Reprehenderit aute quis cillum et reprehenderit exercitation adipisicing elit cillum anim voluptate. Deserunt mollit dolore tempor ullamco minim deserunt labore. Nostrud proident enim dolore aute consectetur eu dolor veniam ad duis est velit.",
    rating: {
      rate: 1.25,
      count: 313,
    },
    discount: 15,
    mrp: 3112,
    category: "hair",
    price: "2645",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e096d7d5dd2af93341d2",
    title: "ullamco qui qui",
    description:
      "Qui officia consectetur consectetur ad ipsum aliqua id nulla. Et dolore minim velit mollit. Aliqua irure consequat magna qui in mollit. Anim elit aute dolore excepteur deserunt. Qui esse nisi id amet ex minim excepteur nulla velit. Quis laborum irure ipsum ad qui exercitation eu nostrud enim tempor adipisicing.",
    rating: {
      rate: 1.68,
      count: 124,
    },
    discount: 5,
    mrp: 3313,
    category: "natural",
    price: "3147",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e09693b53dcc14b8764e",
    title: "sit nisi dolore",
    description:
      "Ad Lorem excepteur officia aliquip id quis tempor aliquip deserunt sint magna occaecat esse. Non ex aute magna eiusmod officia non sint ipsum enim qui proident. Dolor qui in ipsum proident enim. Do irure dolore sunt culpa. Cillum ut mollit pariatur proident excepteur. Labore occaecat incididunt pariatur anim in.",
    rating: {
      rate: 2.04,
      count: 625,
    },
    discount: 10,
    mrp: 8370,
    category: "skin",
    price: "7533",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e096d8c2a631921ac84b",
    title: "ullamco sunt ad",
    description:
      "Culpa laborum quis eu adipisicing sint velit dolor. Ex magna fugiat Lorem nostrud aliquip eu Lorem tempor ut tempor enim quis et sit. Dolore consectetur est laborum consequat fugiat tempor pariatur labore in quis aliqua. Reprehenderit ad sit eu consequat consequat qui. Velit minim sit anim aliquip et sunt cillum non excepteur irure. Pariatur quis id ex aliquip aliqua consectetur qui sunt aute.",
    rating: {
      rate: 1.67,
      count: 306,
    },
    discount: 10,
    mrp: 2574,
    category: "skin",
    price: "2317",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/3/13967ab8904245700454_1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e0964fec03c1a1085cf5",
    title: "esse laborum adipisicing",
    description:
      "Deserunt excepteur ipsum velit magna enim occaecat occaecat eiusmod Lorem aute mollit in eu occaecat. Minim aute aliqua exercitation magna proident adipisicing anim sint. Quis sit cupidatat pariatur dolore ipsum esse. Lorem exercitation duis voluptate aliquip sunt exercitation ut excepteur nostrud est fugiat officia duis. Ut culpa aliqua magna laborum deserunt consequat qui aliqua anim. Deserunt minim magna sunt dolore dolor mollit.",
    rating: {
      rate: 4.27,
      count: 718,
    },
    discount: 15,
    mrp: 9087,
    category: "makeup",
    price: "7724",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e096aa2c9f9eb8fd9aee",
    title: "reprehenderit aliquip duis",
    description:
      "Quis consequat veniam ipsum amet qui dolor anim reprehenderit proident. Non eiusmod aliquip magna cupidatat. Sint pariatur culpa fugiat ea fugiat quis elit voluptate. Consequat veniam magna ea nulla qui. Adipisicing tempor nostrud est adipisicing excepteur occaecat exercitation voluptate aute occaecat officia do in. Amet reprehenderit proident ullamco ad cillum ullamco officia sit velit.",
    rating: {
      rate: 3.05,
      count: 247,
    },
    discount: 10,
    mrp: 343,
    category: "natural",
    price: "309",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e0962519cb2f01b5ba67",
    title: "sunt mollit cupidatat",
    description:
      "Ipsum sint magna aute anim. Duis aute irure esse veniam sunt eiusmod deserunt id mollit nostrud ut. Fugiat id enim minim cillum pariatur. Elit mollit adipisicing quis labore labore tempor anim reprehenderit reprehenderit reprehenderit. Lorem amet minim veniam ea commodo cillum. Ipsum laboris anim culpa enim et.",
    rating: {
      rate: 2.14,
      count: 767,
    },
    discount: 15,
    mrp: 521,
    category: "natural",
    price: "443",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096601bf1b9b8fc41c0",
    title: "fugiat reprehenderit sunt",
    description:
      "Proident enim ut proident et excepteur cillum culpa dolore laboris reprehenderit. Duis fugiat aliqua eu ullamco Lorem fugiat dolor non. Laboris sunt quis magna qui nostrud. Aute laboris sint aliqua mollit quis sunt commodo quis ipsum labore laboris. Cupidatat id laboris minim excepteur incididunt minim. Id ipsum in nisi elit.",
    rating: {
      rate: 3.02,
      count: 287,
    },
    discount: 5,
    mrp: 6367,
    category: "natural",
    price: "6049",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e096f0e946b65ab8efe2",
    title: "consequat qui reprehenderit",
    description:
      "Ea ex non velit ex mollit pariatur culpa magna. Adipisicing dolore occaecat culpa excepteur Lorem nisi. Ex culpa aliqua cupidatat anim. Consequat commodo deserunt ullamco veniam do nisi. Anim in ea esse tempor consequat dolor quis fugiat dolore consequat nostrud. Duis ullamco in officia sunt est elit elit est eiusmod eu incididunt consequat proident excepteur.",
    rating: {
      rate: 4.51,
      count: 601,
    },
    discount: 10,
    mrp: 267,
    category: "skin",
    price: "240",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e096c5a6b410d71aa2f3",
    title: "ex incididunt id",
    description:
      "Aliqua amet culpa voluptate in fugiat sint pariatur incididunt sint officia. Laboris nostrud nulla do magna dolor labore. Nulla sunt amet veniam veniam labore amet exercitation esse dolore. Lorem dolore do laboris amet sit enim. Reprehenderit cillum id labore consequat consectetur aute elit nostrud minim veniam velit consectetur non. Excepteur laboris quis in laborum enim nulla non aute Lorem ad irure veniam id consectetur.",
    rating: {
      rate: 1.96,
      count: 199,
    },
    discount: 20,
    mrp: 9148,
    category: "skin",
    price: "7318",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Biotique",
  },
  {
    id: "6375e096336f76066551aa21",
    title: "tempor et anim",
    description:
      "Ad et dolor ipsum laborum sunt laboris fugiat laborum ad aliquip. Ut quis ut et occaecat pariatur ad adipisicing. Pariatur ullamco laboris excepteur anim. Exercitation consequat sunt elit deserunt enim. Mollit ea cillum aliquip deserunt labore ex labore adipisicing. Exercitation et eu ut duis laboris ut id elit ea irure.",
    rating: {
      rate: 2.57,
      count: 623,
    },
    discount: 10,
    mrp: 7434,
    category: "hair",
    price: "6691",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711924_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e0968da2999a14439b3d",
    title: "magna tempor aute",
    description:
      "Ullamco sunt consectetur ullamco pariatur irure consectetur deserunt et duis cillum in. Velit incididunt enim labore consectetur magna est dolore laborum velit. Mollit aliqua ut eu veniam laboris id sunt irure eu sit. Pariatur consequat incididunt tempor sint culpa eiusmod commodo voluptate. Tempor dolore do in in mollit laborum. Id in aliqua pariatur ut in aliquip adipisicing enim labore magna cupidatat.",
    rating: {
      rate: 4.61,
      count: 312,
    },
    discount: 15,
    mrp: 4578,
    category: "hair",
    price: "3891",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "Biotique",
  },
  {
    id: "6375e09695ce684a8389e4bf",
    title: "eiusmod velit elit",
    description:
      "Esse ad exercitation laboris enim cillum eiusmod. Minim ad anim ea nostrud veniam ex consectetur est sit. Minim irure aliqua eiusmod enim et excepteur voluptate eu sit esse Lorem consequat ea. Qui aute adipisicing nostrud do ipsum non eiusmod ipsum proident excepteur ut. Cillum non esse sint enim laborum ipsum exercitation amet anim eu. Commodo reprehenderit ullamco mollit consequat elit minim id duis aliqua occaecat sunt.",
    rating: {
      rate: 1.13,
      count: 725,
    },
    discount: 5,
    mrp: 499,
    category: "appliances",
    price: "474",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/b/4/b4d615aNYKAC00000424n_1.jpg",
    brand: "Biotique",
  },
  {
    id: "6375e096da4db3379fd2e96f",
    title: "qui veniam laborum",
    description:
      "Duis aliquip aliqua velit laboris in ad reprehenderit laboris mollit in reprehenderit laboris. Sint labore irure anim proident ullamco ipsum. Non enim ea labore tempor labore esse mollit quis aliqua amet sunt elit commodo nisi. Cillum duis in pariatur deserunt sit ut culpa ad proident nostrud voluptate ipsum cupidatat esse. Sint proident commodo voluptate incididunt id commodo esse. Anim exercitation anim pariatur officia nostrud fugiat ea dolor sunt.",
    rating: {
      rate: 1.41,
      count: 540,
    },
    discount: 10,
    mrp: 9551,
    category: "makeup",
    price: "8596",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/f/a/face-primer.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096f53fddb3979eca64",
    title: "commodo fugiat duis",
    description:
      "Eiusmod consectetur eu magna labore mollit velit. Magna quis enim commodo consequat duis exercitation aute. Aliqua mollit nulla deserunt ullamco ut velit cupidatat fugiat. Excepteur ad sunt dolore eu nulla est sint nisi reprehenderit exercitation deserunt. Esse minim duis Lorem culpa minim amet. Sit consectetur occaecat laboris amet deserunt et irure.",
    rating: {
      rate: 3.44,
      count: 267,
    },
    discount: 15,
    mrp: 8037,
    category: "hair",
    price: "6831",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e09683b3b4dd7d19907b",
    title: "aliqua consequat aliquip",
    description:
      "Minim nulla sunt ipsum cillum eu. Consectetur reprehenderit et consequat laboris consectetur laboris ullamco. Excepteur consectetur nulla consequat ex voluptate ad dolor dolor nostrud nulla ipsum non. Magna quis ut Lorem culpa. Lorem aute irure id ex. Proident aliqua do esse enim cillum consequat.",
    rating: {
      rate: 1.84,
      count: 270,
    },
    discount: 20,
    mrp: 4622,
    category: "natural",
    price: "3698",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/b/4/b4d615aNYKAC00000424n_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e0961db8535b9ae4998f",
    title: "officia irure aliqua",
    description:
      "Cillum anim culpa aliqua id veniam culpa sit exercitation nulla occaecat proident reprehenderit. Enim amet commodo fugiat nisi consectetur non. Ut laborum amet veniam in. Id velit quis non occaecat dolore laborum labore sunt pariatur sit pariatur nostrud ullamco. Proident occaecat duis dolor et incididunt laborum anim velit et proident. Sit deserunt sit velit commodo qui dolor dolor ut consequat.",
    rating: {
      rate: 2.94,
      count: 993,
    },
    discount: 10,
    mrp: 8225,
    category: "skin",
    price: "7403",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/3/13967ab8904245700454_1.jpg",
    brand: "The Face Shop",
  },
  {
    id: "6375e096512e7cddd795a8e8",
    title: "minim commodo velit",
    description:
      "Occaecat labore fugiat nostrud dolore Lorem ut non qui id laborum magna sunt deserunt duis. Excepteur nostrud ea cupidatat commodo. Culpa velit consectetur non enim. Amet aliquip deserunt amet veniam in consequat quis excepteur sit magna. Aliquip reprehenderit aliquip sint veniam consequat eiusmod. Elit occaecat do excepteur cupidatat esse anim consequat enim veniam.",
    rating: {
      rate: 1.07,
      count: 996,
    },
    discount: 20,
    mrp: 2827,
    category: "natural",
    price: "2262",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e096e69ae4c48ca16755",
    title: "pariatur voluptate dolor",
    description:
      "Veniam in commodo consectetur cillum dolore esse consequat tempor quis et dolor ut tempor aute. Ullamco sunt ut esse ullamco dolore magna non tempor id. Sunt duis aliquip laboris nulla esse nisi id tempor anim. Adipisicing esse ex velit irure qui proident do sit consequat fugiat veniam sit. Aute in commodo deserunt voluptate non anim laboris qui est nulla voluptate minim incididunt. Lorem duis pariatur irure non cupidatat quis esse sint et adipisicing ad Lorem fugiat.",
    rating: {
      rate: 4.21,
      count: 861,
    },
    discount: 5,
    mrp: 6641,
    category: "natural",
    price: "6309",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711924_1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e09629d1c94555cd3e69",
    title: "non pariatur irure",
    description:
      "Magna in qui voluptate qui eiusmod sint commodo ipsum ipsum. Aute proident duis ipsum aliquip Lorem tempor aute ullamco enim sint minim voluptate. Elit sunt ut sunt nisi nostrud. Non minim amet ex commodo fugiat commodo deserunt elit et nulla esse qui. Fugiat amet aliquip ut cupidatat et culpa aliqua tempor et pariatur ut commodo labore est. Qui voluptate ut mollit aliqua dolore enim officia.",
    rating: {
      rate: 1.94,
      count: 280,
    },
    discount: 20,
    mrp: 102,
    category: "appliances",
    price: "82",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/3/13967ab8904245700454_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096a593f6f14ac7d67c",
    title: "deserunt voluptate irure",
    description:
      "Laboris magna in aliquip nulla aute duis dolor ex. Elit ullamco duis duis officia. Est aliquip reprehenderit do do deserunt ipsum excepteur irure ullamco Lorem. Aliquip enim aute occaecat elit consectetur nisi exercitation ullamco adipisicing ex irure est. Sit sint adipisicing sint officia ullamco labore enim incididunt sint id magna pariatur fugiat cillum. Sint labore do ipsum sunt id velit.",
    rating: {
      rate: 3.18,
      count: 784,
    },
    discount: 5,
    mrp: 8456,
    category: "skin",
    price: "8033",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e0965ac3649891adf13a",
    title: "qui minim et",
    description:
      "Dolor aute et veniam ipsum. Consequat enim officia amet qui. Aliquip culpa voluptate minim dolore et velit eiusmod incididunt veniam sit. Fugiat sit sint nostrud dolore aliqua nostrud minim aute est mollit labore et. Eu fugiat labore veniam voluptate proident duis sunt cupidatat ad ullamco aliqua est consectetur. Laborum proident consectetur id anim Lorem fugiat aliquip in voluptate ea culpa adipisicing cupidatat.",
    rating: {
      rate: 3.19,
      count: 920,
    },
    discount: 20,
    mrp: 3756,
    category: "makeup",
    price: "3005",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/b/4/b4d615aNYKAC00000424n_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e09685a603e18e57df79",
    title: "enim laborum non",
    description:
      "Velit laborum ad ex id non et aute qui ad dolor ut qui. Sunt qui anim voluptate incididunt ea sint tempor sint. Deserunt esse aute nisi ad culpa quis deserunt mollit nisi Lorem dolore aute id. Labore eu laborum labore qui pariatur anim veniam laborum velit. Et officia fugiat mollit excepteur amet cillum amet quis laborum duis ex laboris. Magna officia Lorem sit laboris do mollit velit proident minim.",
    rating: {
      rate: 4.14,
      count: 1000,
    },
    discount: 20,
    mrp: 6804,
    category: "natural",
    price: "5443",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e0960b84533379b6e1ce",
    title: "amet incididunt excepteur",
    description:
      "Pariatur est sit veniam est Lorem. Sunt quis esse minim excepteur veniam. Aliqua in nostrud consequat aliqua labore aute sint officia Lorem nulla culpa ex. Occaecat officia magna voluptate aliqua occaecat esse mollit eiusmod ut ea nulla nulla. Eu magna officia dolore incididunt labore dolore veniam magna labore in duis elit eu. Aliqua Lorem cupidatat mollit eiusmod cupidatat labore proident occaecat officia elit.",
    rating: {
      rate: 4.94,
      count: 235,
    },
    discount: 10,
    mrp: 1724,
    category: "hair",
    price: "1552",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e0964c3f2b49572de7c0",
    title: "nisi dolor labore",
    description:
      "Commodo dolore laboris laboris laboris. Irure laborum ut nostrud esse sit deserunt occaecat tempor fugiat cillum est consequat irure. Sunt sint esse veniam magna amet fugiat cupidatat ut exercitation Lorem qui Lorem ipsum. Labore esse duis laborum dolore culpa in non culpa nostrud pariatur. Do excepteur qui excepteur mollit sint fugiat. Est exercitation sint duis tempor velit incididunt dolor exercitation esse magna exercitation non et.",
    rating: {
      rate: 3.37,
      count: 122,
    },
    discount: 20,
    mrp: 3462,
    category: "natural",
    price: "2770",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/tr:w-220,h-220,cm-pad_resize/8/9/8904245712938_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096ad99616df2a0a42c",
    title: "proident reprehenderit aliquip",
    description:
      "Do quis non ut commodo. Do esse consectetur aliqua laborum sint consequat ex eu do laboris cillum laborum. Exercitation anim occaecat fugiat do sint sunt fugiat. Minim exercitation elit aliquip anim ipsum veniam enim dolor nulla deserunt consectetur. Id incididunt do veniam anim fugiat commodo ex ullamco magna anim minim nostrud. Reprehenderit quis non enim eiusmod ullamco ea id cupidatat incididunt commodo sint aute.",
    rating: {
      rate: 2.92,
      count: 492,
    },
    discount: 5,
    mrp: 6370,
    category: "skin",
    price: "6052",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e0969c87daa464b778d9",
    title: "minim anim deserunt",
    description:
      "Cillum deserunt pariatur ex qui sit Lorem pariatur esse in duis adipisicing proident nostrud. Esse commodo ex qui esse non. Officia nisi quis Lorem exercitation ex. Veniam ad consequat deserunt proident nisi ad irure ullamco. Mollit irure duis ex quis reprehenderit nisi. Anim veniam commodo occaecat esse amet incididunt.",
    rating: {
      rate: 2.93,
      count: 622,
    },
    discount: 15,
    mrp: 9620,
    category: "skin",
    price: "8177",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096de2eced807f08978",
    title: "minim quis exercitation",
    description:
      "Reprehenderit ea esse ipsum consectetur eu elit ex commodo cupidatat incididunt. Aliqua ad occaecat do irure fugiat. Magna occaecat aliqua sint aliqua eiusmod in eiusmod nostrud velit esse adipisicing magna cupidatat. Quis eu culpa culpa nulla consequat id nulla. Pariatur amet ad sint consequat dolore quis Lorem enim. Excepteur nisi eu minim commodo enim.",
    rating: {
      rate: 3.36,
      count: 281,
    },
    discount: 15,
    mrp: 9074,
    category: "appliances",
    price: "7713",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711924_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096fc1ef4349e4c453c",
    title: "aute laborum enim",
    description:
      "Ut elit adipisicing qui laborum dolore cillum non ad ea minim. Voluptate veniam elit fugiat anim magna quis tempor exercitation cillum. Consequat dolore nostrud aliquip reprehenderit voluptate eiusmod est laborum dolor. Nisi Lorem enim labore do do ipsum magna irure. Excepteur tempor et do adipisicing aute aute et anim qui elit irure commodo aute nulla. Deserunt nulla consectetur pariatur est.",
    rating: {
      rate: 2.83,
      count: 992,
    },
    discount: 10,
    mrp: 8933,
    category: "natural",
    price: "8040",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e09605c01b58efc3a091",
    title: "pariatur dolore fugiat",
    description:
      "Ipsum in esse laborum deserunt fugiat. Culpa eiusmod nisi sint occaecat non nisi consequat velit officia consequat exercitation anim nisi nostrud. Proident sunt est qui dolore aliqua occaecat sit id ex. Tempor fugiat esse nostrud exercitation veniam adipisicing commodo ut culpa do. Est pariatur Lorem ad irure nulla irure occaecat consectetur in non. Tempor nisi commodo non ipsum dolore.",
    rating: {
      rate: 2.42,
      count: 323,
    },
    discount: 15,
    mrp: 1245,
    category: "natural",
    price: "1058",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/tr:w-220,h-220,cm-pad_resize/8/9/8904245712938_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e0968226cd3ae31baeb6",
    title: "velit laborum occaecat",
    description:
      "Occaecat dolore eiusmod ipsum non fugiat. Occaecat ut labore aliqua nostrud amet magna deserunt amet aliqua veniam. Sit incididunt ut non non quis proident cupidatat labore adipisicing ipsum ad commodo occaecat. Dolore veniam deserunt commodo in sit velit voluptate esse laboris. Occaecat amet non amet aliquip commodo eu quis incididunt dolor magna. Et ex qui deserunt consectetur amet duis irure consequat dolor sit est.",
    rating: {
      rate: 3.66,
      count: 997,
    },
    discount: 15,
    mrp: 5541,
    category: "appliances",
    price: "4710",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e0968b043d31791e4b19",
    title: "laboris sit ea",
    description:
      "Deserunt elit ea occaecat culpa nulla ut magna excepteur id exercitation consectetur. Non adipisicing sit voluptate commodo. Reprehenderit pariatur eu do consequat nulla ex nostrud cillum nulla exercitation. Officia dolor pariatur deserunt mollit consequat in officia. Eu amet voluptate nisi id ut. Consectetur est pariatur laboris occaecat adipisicing commodo mollit aute dolor et ad magna cupidatat dolore.",
    rating: {
      rate: 2.01,
      count: 776,
    },
    discount: 10,
    mrp: 8535,
    category: "appliances",
    price: "7682",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096dd33966bdffad806",
    title: "ea consequat laborum",
    description:
      "Sint aute voluptate tempor id. Reprehenderit excepteur et proident pariatur consectetur est et. Commodo ex qui enim aute. Nostrud est cillum dolore aliqua nulla excepteur est mollit qui dolor nulla enim esse enim. Cupidatat consequat aliquip sunt tempor ipsum. Culpa proident adipisicing exercitation ut sunt ad reprehenderit aliquip veniam.",
    rating: {
      rate: 2.09,
      count: 891,
    },
    discount: 15,
    mrp: 6589,
    category: "hair",
    price: "5601",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e0962457fd23b1b5e4ac",
    title: "aliqua id Lorem",
    description:
      "Minim aliquip consectetur excepteur ex. Eu elit quis eu nostrud nisi aute labore minim anim irure esse. Enim reprehenderit ipsum sit eiusmod consectetur pariatur in labore ipsum tempor laborum exercitation. Nisi ex amet in est voluptate id culpa fugiat exercitation irure deserunt. Qui veniam velit occaecat non in magna et. Pariatur eiusmod tempor tempor excepteur incididunt ullamco laborum aliquip.",
    rating: {
      rate: 4.12,
      count: 962,
    },
    discount: 15,
    mrp: 6918,
    category: "skin",
    price: "5880",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096132f093b273e3969",
    title: "in do et",
    description:
      "Magna veniam elit aute ad eiusmod duis ullamco pariatur exercitation ad est magna cillum. Occaecat fugiat nostrud aliqua pariatur consequat. Nulla magna est ea enim. Sint ullamco ex proident aliqua sunt Lorem non proident irure. Sint sunt ea tempor ut cupidatat in occaecat eiusmod. Aliquip consequat minim Lorem minim quis nulla ea commodo non exercitation pariatur incididunt nulla.",
    rating: {
      rate: 1.67,
      count: 540,
    },
    discount: 5,
    mrp: 5980,
    category: "hair",
    price: "5681",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e096fe92ac85e6e80125",
    title: "magna ut irure",
    description:
      "Proident voluptate voluptate adipisicing magna sint sint. Ullamco sunt occaecat nisi tempor cillum ea voluptate. Nisi quis nostrud non tempor Lorem ex non pariatur commodo ea ex labore quis. Officia fugiat incididunt magna nostrud commodo dolore laborum amet ullamco voluptate deserunt. Sunt ipsum sit ullamco deserunt minim. Sint nostrud et fugiat ex magna.",
    rating: {
      rate: 3.06,
      count: 745,
    },
    discount: 5,
    mrp: 9220,
    category: "natural",
    price: "8759",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711924_1.jpg",
    brand: "Biotique",
  },
  {
    id: "6375e096727514bd70b19b21",
    title: "ad cupidatat excepteur",
    description:
      "Pariatur nulla ut labore culpa fugiat pariatur magna. Non voluptate nisi dolor cillum do. Ex pariatur exercitation aliquip et mollit aliquip est minim ut pariatur occaecat sunt laboris dolor. Tempor Lorem ullamco esse et esse minim sit. Sunt cillum id qui reprehenderit Lorem quis dolore Lorem non tempor nostrud sunt. Id exercitation velit sunt consequat in sint sint adipisicing laboris et amet incididunt nulla.",
    rating: {
      rate: 2.28,
      count: 468,
    },
    discount: 15,
    mrp: 991,
    category: "makeup",
    price: "842",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Innisfree",
  },
  {
    id: "6375e0963a144f85fa20f51e",
    title: "cupidatat aute aliquip",
    description:
      "Lorem dolore ex ex Lorem. Do enim sunt qui do qui esse magna irure ipsum enim non nisi ipsum. Deserunt laboris ullamco sunt culpa ea sit mollit quis. Proident dolore est laboris exercitation culpa laborum labore do sunt mollit sint est dolor minim. Commodo Lorem ut laboris reprehenderit sit. Tempor dolor eiusmod sint ea id.",
    rating: {
      rate: 1.34,
      count: 179,
    },
    discount: 10,
    mrp: 3522,
    category: "appliances",
    price: "3170",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Innisfree",
  },
  {
    id: "6375e0960e572a078dd05649",
    title: "laboris aute deserunt",
    description:
      "Deserunt non esse et ea proident deserunt excepteur minim velit. Enim irure enim reprehenderit deserunt. In ex ullamco ad duis aute minim excepteur in cupidatat veniam occaecat fugiat. Cupidatat Lorem in labore deserunt voluptate sunt sit nostrud nulla consequat consequat. Pariatur velit et magna nulla ex mollit sint. Culpa tempor tempor in eu dolor ipsum irure commodo.",
    rating: {
      rate: 3.34,
      count: 512,
    },
    discount: 15,
    mrp: 3900,
    category: "hair",
    price: "3315",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096b230eefdfab01a2c",
    title: "elit incididunt aliquip",
    description:
      "Aute reprehenderit eu nulla veniam nulla officia pariatur proident. Cillum deserunt aute ea consectetur pariatur commodo duis amet nostrud cupidatat reprehenderit. Voluptate excepteur mollit tempor eu occaecat. Id qui veniam anim cupidatat. Officia consectetur culpa sint mollit esse mollit in tempor eu ex incididunt. Quis proident commodo ipsum magna est nulla tempor veniam.",
    rating: {
      rate: 4.93,
      count: 423,
    },
    discount: 15,
    mrp: 4910,
    category: "hair",
    price: "4174",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e096e0641755c1d55af4",
    title: "minim et cupidatat",
    description:
      "Non officia ad cillum et reprehenderit ea non magna. Exercitation ad pariatur labore culpa ut anim ex. Exercitation sint deserunt excepteur amet reprehenderit aliqua anim Lorem elit. Sint ad elit sunt laborum quis irure culpa minim. Elit nulla occaecat ullamco laboris. Aute minim adipisicing esse consequat sint velit ipsum aute anim officia.",
    rating: {
      rate: 4.38,
      count: 551,
    },
    discount: 20,
    mrp: 8968,
    category: "hair",
    price: "7174",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096f4a7f658290cb332",
    title: "labore cillum excepteur",
    description:
      "Elit ut tempor reprehenderit sint. Sit occaecat Lorem reprehenderit veniam commodo nulla dolore dolore adipisicing eu nulla sit. Voluptate deserunt aute non et eu ex pariatur qui consectetur officia sunt eiusmod qui. Consequat est esse sit ut. Deserunt tempor amet non ea duis laboris in Lorem laborum sint. Enim ad labore commodo occaecat incididunt dolor nulla ex tempor adipisicing mollit laborum cillum.",
    rating: {
      rate: 2.05,
      count: 775,
    },
    discount: 5,
    mrp: 1188,
    category: "hair",
    price: "1129",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e096911cee2fcc30e2ae",
    title: "id magna adipisicing",
    description:
      "Irure ad deserunt ullamco incididunt occaecat magna enim incididunt pariatur reprehenderit magna anim. Magna nostrud deserunt incididunt dolor occaecat. Cupidatat eiusmod voluptate duis esse mollit sunt elit duis id nisi id. Cillum dolor ipsum sint ex eu commodo pariatur commodo. Amet do labore tempor et do cillum ipsum. Mollit ipsum duis et exercitation incididunt Lorem exercitation dolor irure nisi aute do duis.",
    rating: {
      rate: 3.22,
      count: 626,
    },
    discount: 15,
    mrp: 110,
    category: "skin",
    price: "94",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e096081dbf982c4e2dd4",
    title: "occaecat est in",
    description:
      "Ipsum dolore sint aliqua nulla ut labore est consequat laboris officia excepteur minim et. Dolor eiusmod aliquip consequat est eiusmod ea est enim esse reprehenderit quis labore in. Eiusmod eu dolor aliqua eu ad ut quis magna aliqua dolor nisi veniam dolor nulla. Nisi aliqua aute qui reprehenderit eiusmod velit aute fugiat. Sint adipisicing aute labore esse consectetur excepteur officia amet commodo dolor ullamco tempor in cillum. Tempor sunt veniam nisi fugiat id nulla est dolor magna sunt occaecat exercitation cupidatat.",
    rating: {
      rate: 1.02,
      count: 805,
    },
    discount: 5,
    mrp: 286,
    category: "hair",
    price: "272",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/f/a/face-primer.jpg",
    brand: "Innisfree",
  },
  {
    id: "6375e096f56da590e6d352c1",
    title: "mollit eiusmod culpa",
    description:
      "Id esse ad velit tempor. Laborum non enim aute ea labore est aliqua. Qui veniam laboris dolor nostrud. Nisi deserunt laborum enim id incididunt commodo tempor enim aute in velit ex laboris ea. Adipisicing nisi laboris magna velit in sint. Do nostrud ipsum id elit reprehenderit non minim tempor velit nulla mollit ex amet sit.",
    rating: {
      rate: 1.87,
      count: 211,
    },
    discount: 10,
    mrp: 229,
    category: "hair",
    price: "206",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/a/a/aaa63f68904245707316_rev1.jpg",
    brand: "Huda Beauty",
  },
  {
    id: "6375e09665e70705a0e07e6c",
    title: "veniam laborum irure",
    description:
      "Laborum culpa elit dolor veniam excepteur anim velit reprehenderit voluptate eiusmod commodo mollit. Nulla ipsum non mollit proident consectetur veniam cillum elit irure dolor ad magna incididunt et. Minim duis eu nulla et mollit. Duis nulla amet cupidatat nisi magna. Do amet fugiat nostrud quis veniam exercitation pariatur esse. Magna irure exercitation irure enim commodo deserunt et aliquip ut sit.",
    rating: {
      rate: 1.08,
      count: 771,
    },
    discount: 20,
    mrp: 6618,
    category: "skin",
    price: "5294",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096c0f60ba95a1bbc2b",
    title: "aliqua nisi labore",
    description:
      "Fugiat Lorem nisi labore magna duis ullamco ad velit tempor duis laborum. Nostrud cillum Lorem consequat excepteur Lorem ullamco ut aliquip adipisicing eiusmod incididunt labore. Proident consequat aute ad nisi cillum sit minim elit adipisicing amet ad consectetur. Proident adipisicing velit amet sint. Laboris ea ullamco dolor culpa do excepteur eu eu tempor nulla excepteur. Pariatur exercitation eu consectetur elit consectetur cillum tempor laborum adipisicing labore nostrud sunt quis.",
    rating: {
      rate: 2.83,
      count: 961,
    },
    discount: 5,
    mrp: 9448,
    category: "hair",
    price: "8976",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Lakme",
  },
  {
    id: "6375e096391c3288402bbdd0",
    title: "nostrud culpa eu",
    description:
      "Aute magna laboris elit minim sunt est laborum. Cillum veniam ipsum minim cupidatat sit aliquip irure consectetur dolor. Deserunt officia exercitation adipisicing ut culpa culpa et amet excepteur aliqua. Fugiat aliquip cupidatat sint anim id est. Aliquip ex laboris qui consequat. Consectetur pariatur amet dolore labore consectetur anim nisi Lorem ea incididunt aute excepteur elit sunt.",
    rating: {
      rate: 3.96,
      count: 408,
    },
    discount: 15,
    mrp: 2858,
    category: "natural",
    price: "2429",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096ae1cf8914bb0c43c",
    title: "quis do reprehenderit",
    description:
      "Et occaecat deserunt consequat aliqua fugiat eiusmod ut excepteur nulla laboris velit. Exercitation aliquip nostrud Lorem mollit. Pariatur voluptate consectetur incididunt nostrud veniam pariatur laborum amet laboris tempor dolore exercitation duis. Nisi laboris excepteur cupidatat pariatur commodo magna anim do incididunt consectetur irure aliquip occaecat. Incididunt laboris Lorem nulla ad cupidatat voluptate. Id anim excepteur nostrud dolore sunt quis pariatur non esse sint est.",
    rating: {
      rate: 4.29,
      count: 496,
    },
    discount: 20,
    mrp: 399,
    category: "natural",
    price: "319",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/f/a/face-primer.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e096b187880830d5411f",
    title: "velit incididunt velit",
    description:
      "Exercitation pariatur tempor quis est dolore irure Lorem elit eiusmod dolor aute. Ullamco laborum enim voluptate consectetur quis eu consequat labore sint velit proident nulla. Incididunt dolor fugiat minim pariatur eiusmod non nostrud laborum pariatur tempor dolor laborum elit. Proident velit occaecat et ut esse. Elit cupidatat est culpa fugiat labore. Adipisicing cupidatat Lorem ad velit amet incididunt excepteur elit.",
    rating: {
      rate: 2.83,
      count: 920,
    },
    discount: 10,
    mrp: 845,
    category: "appliances",
    price: "761",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/c/3/c31c1eb8904245710057_1.jpg",
    brand: "Innisfree",
  },
  {
    id: "6375e096836decf0cc2339dc",
    title: "ipsum commodo culpa",
    description:
      "Proident qui esse dolor consectetur laboris exercitation fugiat voluptate reprehenderit velit deserunt irure ea. Ullamco sit enim nostrud aliquip commodo veniam exercitation consequat. Consequat aliqua do qui pariatur esse mollit amet. Sint eiusmod voluptate proident eiusmod in laboris quis irure nulla anim consectetur quis. In dolore deserunt ea ad. Commodo reprehenderit aliquip voluptate officia adipisicing anim ea ad dolore occaecat.",
    rating: {
      rate: 2.17,
      count: 871,
    },
    discount: 5,
    mrp: 8983,
    category: "skin",
    price: "8534",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "The Face Shop",
  },
  {
    id: "6375e096525eb3fb43eb214b",
    title: "sit quis elit",
    description:
      "Duis et reprehenderit nostrud pariatur aute nulla irure fugiat labore aliquip. Mollit tempor ea eu deserunt cillum reprehenderit ex magna culpa mollit. Proident et ullamco consectetur ex cupidatat adipisicing consectetur consequat. Non ut nulla anim sit ex ut adipisicing aliquip veniam deserunt aute. Do adipisicing enim sint culpa aute fugiat do in mollit. Excepteur laboris consequat non dolore ad mollit qui.",
    rating: {
      rate: 1.37,
      count: 58,
    },
    discount: 10,
    mrp: 4170,
    category: "skin",
    price: "3753",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e09693cee7acef2cc622",
    title: "tempor id culpa",
    description:
      "Lorem enim aliqua aute aliqua cupidatat laborum tempor voluptate ex sit sunt. Excepteur proident magna do irure eu et sint sint exercitation. Voluptate labore amet sit reprehenderit nisi. Dolore qui eu cupidatat est do dolore magna ipsum ad quis do aute. Tempor incididunt consequat ipsum nulla aute incididunt dolor excepteur id sunt. Deserunt labore nisi mollit mollit excepteur excepteur sint proident velit mollit ullamco pariatur est cupidatat.",
    rating: {
      rate: 2.25,
      count: 202,
    },
    discount: 10,
    mrp: 4801,
    category: "natural",
    price: "4321",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/b/4/b4d615aNYKAC00000424n_1.jpg",
    brand: "Nykaa Naturals",
  },
  {
    id: "6375e096c97651754cb366fc",
    title: "officia consectetur ex",
    description:
      "Nisi elit cupidatat sint exercitation. Eu sit nostrud sint voluptate cillum do consectetur duis ullamco. Sint reprehenderit nulla aliqua ea sint occaecat exercitation ut. Nostrud magna non elit elit consectetur aliqua et incididunt enim et. Adipisicing fugiat reprehenderit non ut eiusmod duis reprehenderit est proident. Deserunt ad ad in eu est in ipsum ad dolor irure pariatur sunt ut.",
    rating: {
      rate: 2.79,
      count: 936,
    },
    discount: 15,
    mrp: 1252,
    category: "skin",
    price: "1064",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e09663132b9ecac6f431",
    title: "ex ipsum labore",
    description:
      "Esse dolor pariatur nostrud qui proident eu et ipsum do in velit sint eu. Ipsum irure est in aliquip ullamco qui est excepteur. Proident non nostrud non do voluptate. Incididunt enim consequat qui ea ipsum nisi cupidatat sunt incididunt dolor commodo consequat exercitation. Sint duis laborum fugiat dolor ad dolore voluptate est ea mollit nostrud. Ad commodo do consequat cillum laboris laboris adipisicing qui nisi quis Lorem irure est dolore.",
    rating: {
      rate: 3.84,
      count: 396,
    },
    discount: 20,
    mrp: 6815,
    category: "makeup",
    price: "5452",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "M.A.C",
  },
  {
    id: "6375e0965f4fe23e7bd96850",
    title: "pariatur id nulla",
    description:
      "Occaecat incididunt eiusmod aliquip adipisicing consequat eiusmod cillum ad voluptate do consequat amet eu est. Nisi aute ullamco veniam esse proident in officia sint ea ex voluptate aliqua aliquip. Enim et deserunt occaecat qui. Ea officia commodo consectetur fugiat labore labore voluptate. Eiusmod nulla pariatur eiusmod exercitation. Anim in laborum eiusmod id laboris officia labore cillum sunt esse.",
    rating: {
      rate: 3.86,
      count: 110,
    },
    discount: 10,
    mrp: 4197,
    category: "makeup",
    price: "3777",
    featured: false,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/a/a/aaa63f68904245707316_rev1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e09627a71fbcfe5a2dd5",
    title: "Lorem consectetur voluptate",
    description:
      "Eu mollit ex Lorem amet cupidatat aute aliqua ullamco pariatur est. Qui dolor nulla ex voluptate aute laborum fugiat nostrud sint amet pariatur. Nulla quis veniam deserunt excepteur quis amet culpa fugiat proident. Enim cillum Lorem fugiat elit. Sit officia aute cillum culpa mollit cillum excepteur elit consectetur qui irure laborum. Ipsum ex consequat incididunt aliquip consectetur ad fugiat.",
    rating: {
      rate: 2.94,
      count: 868,
    },
    discount: 15,
    mrp: 9772,
    category: "makeup",
    price: "8306",
    featured: true,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Maybelline New York",
  },
  {
    id: "6375e09687b438a792a2a9ae",
    title: "velit deserunt fugiat",
    description:
      "Mollit officia magna occaecat consectetur anim irure culpa aliquip sint minim dolore. Irure tempor deserunt anim mollit consectetur nostrud. Consequat adipisicing laboris sint nisi aute occaecat tempor nostrud reprehenderit. Aliquip aute ullamco nostrud enim culpa aute nisi. Consectetur cillum enim mollit magna exercitation proident deserunt sunt culpa. Duis veniam aliquip in amet ipsum enim tempor sit consequat.",
    rating: {
      rate: 4.14,
      count: 706,
    },
    discount: 10,
    mrp: 3400,
    category: "appliances",
    price: "3060",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711146_1.jpg",
    brand: "The Body Shop",
  },
  {
    id: "6375e09657b4694dcb64517e",
    title: "pariatur magna consectetur",
    description:
      "Mollit nulla culpa tempor aliquip consectetur aute proident. Excepteur proident irure nisi aute ut pariatur sint pariatur laborum enim. Ipsum qui cillum culpa proident Lorem anim Lorem sit culpa labore minim adipisicing excepteur nostrud. Est est sit aliqua qui labore sit minim occaecat. Tempor in in tempor elit qui aliquip irure consectetur. Esse mollit reprehenderit dolore qui.",
    rating: {
      rate: 2.55,
      count: 558,
    },
    discount: 20,
    mrp: 3860,
    category: "hair",
    price: "3088",
    featured: true,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/1/1/11e9fedNYKAC00000372_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e0961e59d39cfa59ffff",
    title: "voluptate tempor eiusmod",
    description:
      "Ullamco minim voluptate duis aute nisi anim dolore elit proident reprehenderit veniam elit voluptate dolor. Do ea incididunt amet ullamco tempor. Irure consequat minim adipisicing consectetur nostrud amet amet. Anim excepteur incididunt eiusmod irure. Commodo ea sit non veniam proident reprehenderit nulla elit deserunt cupidatat minim ipsum. Ea incididunt laboris labore quis amet cupidatat sunt adipisicing proident qui cupidatat.",
    rating: {
      rate: 4.34,
      count: 1000,
    },
    discount: 15,
    mrp: 2411,
    category: "makeup",
    price: "2049",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/c/8c1e25bNYKAC00000073_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e0968a7e6c24f5c9c2b3",
    title: "minim enim et",
    description:
      "In nulla in dolor labore magna nostrud veniam mollit nulla culpa. Aute officia do minim quis irure do qui Lorem commodo est dolor. Labore enim occaecat reprehenderit consequat ea id fugiat do magna culpa. Sint occaecat est ullamco anim laboris laboris exercitation quis reprehenderit anim enim fugiat enim. Voluptate non duis nisi commodo sint laboris eiusmod velit dolore fugiat. Ipsum non ut dolor Lorem ad ipsum cillum veniam.",
    rating: {
      rate: 1.79,
      count: 740,
    },
    discount: 20,
    mrp: 8039,
    category: "skin",
    price: "6431",
    featured: true,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245706227_1_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096c48e12fbb306f3cd",
    title: "enim culpa magna",
    description:
      "Deserunt pariatur duis et elit. Ut labore consectetur sint sunt ullamco eu laboris officia aliqua Lorem exercitation. Minim do dolore Lorem deserunt aliqua quis anim ullamco nostrud elit. Consequat magna velit exercitation irure nostrud exercitation non eiusmod elit duis labore qui. Excepteur officia in officia reprehenderit sit. Aute dolor laborum consequat irure deserunt dolor duis.",
    rating: {
      rate: 1.35,
      count: 162,
    },
    discount: 20,
    mrp: 7536,
    category: "skin",
    price: "6029",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/f/a/face-primer.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096f645e05d2c5bc8a6",
    title: "ex nulla culpa",
    description:
      "Deserunt enim velit ut ipsum labore eiusmod culpa irure laboris sint amet consequat. Consequat anim fugiat velit eu anim deserunt amet velit est excepteur. Reprehenderit occaecat dolor minim tempor deserunt ullamco elit magna Lorem aute deserunt tempor. Aliqua mollit et tempor mollit ex officia quis aliquip. Eiusmod Lorem cillum mollit officia do nostrud. Labore enim consectetur aute dolor.",
    rating: {
      rate: 2.39,
      count: 909,
    },
    discount: 20,
    mrp: 2671,
    category: "appliances",
    price: "2137",
    featured: false,
    bestseller: true,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/tr:w-220,h-220,cm-pad_resize/8/9/8904245712938_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e096a3e325468d6b1f14",
    title: "adipisicing consectetur sit",
    description:
      "Sit et proident id incididunt laboris sunt occaecat nostrud commodo. Cupidatat non adipisicing proident mollit sit aliqua Lorem aliqua ullamco in. Eiusmod consequat fugiat non cillum voluptate aliquip dolor. Eiusmod dolor aliqua exercitation elit cillum officia commodo proident nisi velit ex deserunt sint pariatur. Lorem duis aliquip mollit aute ad et enim. Labore nisi elit non elit sit commodo in laborum.",
    rating: {
      rate: 2.26,
      count: 141,
    },
    discount: 20,
    mrp: 1167,
    category: "appliances",
    price: "934",
    featured: true,
    bestseller: true,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/n/y/nykac00000142_1.jpg",
    brand: "Kama Ayurveda",
  },
  {
    id: "6375e096a76f25962ebb50ee",
    title: "amet est sint",
    description:
      "Occaecat occaecat irure incididunt cillum voluptate nulla consequat anim cillum. Occaecat commodo mollit nisi fugiat sunt culpa in tempor labore sint occaecat ullamco. Duis consectetur ipsum commodo fugiat. Mollit anim eiusmod do aute amet. Dolore enim fugiat deserunt elit proident non anim eiusmod dolor reprehenderit. Ut sint fugiat Lorem aliquip aute pariatur aliqua ex.",
    rating: {
      rate: 3.54,
      count: 859,
    },
    discount: 5,
    mrp: 5549,
    category: "hair",
    price: "5272",
    featured: false,
    bestseller: false,
    freeGift: true,
    image:
      "https://images-static.nykaa.com/media/catalog/product/e/3/e306feenykac00000240_1.jpg",
    brand: "Nykaa Cosmetics",
  },
  {
    id: "6375e096de0ef4d4b4a3a689",
    title: "amet Lorem mollit",
    description:
      "Adipisicing id ullamco elit pariatur ipsum. Sunt voluptate enim occaecat et labore mollit consectetur elit irure aliquip commodo. Non sint qui sunt enim sit pariatur proident Lorem. Veniam magna anim ullamco in adipisicing aute culpa officia ea dolor sunt. Incididunt quis voluptate cillum nisi occaecat quis in labore excepteur. Ut quis minim sit dolor duis voluptate amet duis magna commodo incididunt velit.",
    rating: {
      rate: 4.85,
      count: 672,
    },
    discount: 5,
    mrp: 7522,
    category: "skin",
    price: "7146",
    featured: false,
    bestseller: false,
    freeGift: false,
    image:
      "https://images-static.nykaa.com/media/catalog/product/8/9/8904245711924_1.jpg",
    brand: "Lakme",
  },
];

export default products;
